<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppsu extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')==3)
            redirect('login');
        
        $this->load->model('m_ppsu');
    }
    
    public function index()
    {
        $rec = $this->m_ppsu->listPpsu();
        
        $data = array(
                    'view'          => 'ppsu/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'PPSU',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->input->post('nama')!=null)
        {
            // insert file
            $config['upload_path']          = './foto/';
            $config['allowed_types']        = 'jpeg|jpg|png|pdf';
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('foto'))
                $foto = $this->upload->data('file_name');
            else
                $foto = null;
            
            $input = array(
                'kelurahan_id'  => $this->input->post('kelurahan_id'),
                'nama'          => $this->input->post('nama'),
                'alamat'        => $this->input->post('alamat'),
                'no_telepon'    => $this->input->post('no_telepon'),
                'ketua'         => $this->input->post('ketua'),
                'foto'          => $foto,
            );
            $id = $this->m_ppsu->insertPpsu($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data PPSU berhasil disimpan');

            redirect(base_url().'ppsu/view/'.$id);
        }
        
        $data = array(
                    'view'          => 'ppsu/create',
                    'judul'         => 'PPSU',
                    'js'            => array('validation'=>'validation','select2'=>'select2'),
                    'css'           => array('select2'=>'select2'),
                    'cmbKelurahan'  => $this->m_ppsu->cmbKelurahan(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function view($id)
    {
        $rec = $this->m_ppsu->viewPpsu($id);
        
        if($this->session->userdata('level_id') == 1 || ($this->session->userdata('level_id')!=1 && $this->session->userdata('kelurahan_id')==$rec->kelurahan_id) )
        {
            $data = array(
                        'view'          => 'ppsu/view',
                        'judul'         => 'PPSU',
                        'js'            => array(),
                        'css'           => array(),
                        'rec'           => $rec,
            );

            $this->load->view('template', $data);
        }
        else
        {
            redirect(base_url().'ppsu');
        }
    }
    
    public function edit($id)
    {
        $rec = $this->m_ppsu->viewPpsu($id);
        
        if($this->session->userdata('level_id') == 1 || ($this->session->userdata('level_id')!=1 && $this->session->userdata('kelurahan_id')==$rec->kelurahan_id) )
        {        
            if($this->input->post('nama')!=null)
            {
                // insert file
                $config['upload_path']          = './foto/';
                $config['allowed_types']        = 'jpeg|jpg|png|pdf';
                $this->load->library('upload', $config);

                if($_FILES['foto']['name'])
                {
                    if($this->upload->do_upload('foto'))
                    {
                        if(file_exists('foto/'.$rec->foto))
                            unlink('foto/'.$rec->foto);

                        $foto = $this->upload->data('file_name');
                    }
                    else
                        $foto = null;
                }
                else
                    $foto = $rec->foto;

                $input = array(
                    'kelurahan_id'  => $this->input->post('kelurahan_id'),
                    'nama'          => $this->input->post('nama'),
                    'alamat'        => $this->input->post('alamat'),
                    'no_telepon'    => $this->input->post('no_telepon'),
                    'ketua'         => $this->input->post('ketua'),
                    'foto'          => $foto,
                );
                $this->m_ppsu->updatePpsu($input, $rec->id);

                $this->session->set_userdata('status', 'info');
                $this->session->set_userdata('pesan', 'Data PPSU berhasil diubah');

                redirect(base_url().'ppsu/view/'.$rec->id);
            }

            $data = array(
                        'view'          => 'ppsu/edit',
                        'judul'         => 'PPSU',
                        'js'            => array('validation'=>'validation','select2'=>'select2'),
                        'css'           => array('select2'=>'select2'),
                        'rec'           => $rec,
                        'cmbKelurahan'       => $this->m_ppsu->cmbKelurahan(),
            );

            $this->load->view('template', $data);
        }
        else
        {
            redirect(base_url().'ppsu');
        }
    }
}