<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_laporan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }
    
    function cmbKelurahan()
    {
        $this->db->select('*');
        $this->db->from('kelurahan');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKode()
    {
        $this->db->select('*');
        $this->db->from('kode');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function getLaporanHarian($kelurahan_id, $kode_id, $tanggal)
    {
        $this->db->select('k.*, s.nama as nama_sumber, kod.nama as nama_kode, kel.nama as nama_kel, p.nama as nama_ppsu, '
                        . 'kel.lurah, kel.nip_lurah, kel.korlap, kel.nip_korlap');
        $this->db->from('kegiatan as k');
        $this->db->join('sumber as s', 'k.sumber_id=s.id', 'left');
        $this->db->join('kode as kod', 'k.kode_id=kod.id', 'left');
        $this->db->join('kelurahan as kel', 'k.kelurahan_id=kel.id', 'left');
        $this->db->join('ppsu as p', 'k.ppsu_id=p.id', 'left');
        $this->db->where('k.kelurahan_id', $kelurahan_id);
        $this->db->where('k.kode_id', $kode_id);
        $this->db->where("DATE_FORMAT(k.tanggal, '%Y-%m-%d')='".$tanggal."'");
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec;
        else
            return null;
    }
    
    function getLaporanBulanan($kelurahan_id, $kode_id, $tanggal)
    {
        $this->db->select('k.*, s.nama as nama_sumber, kod.nama as nama_kode, kel.nama as nama_kel, p.nama as nama_ppsu, '
                        . 'kel.lurah, kel.nip_lurah, kel.korlap, kel.nip_korlap');
        $this->db->from('kegiatan as k');
        $this->db->join('sumber as s', 'k.sumber_id=s.id', 'left');
        $this->db->join('kode as kod', 'k.kode_id=kod.id', 'left');
        $this->db->join('kelurahan as kel', 'k.kelurahan_id=kel.id', 'left');
        $this->db->join('ppsu as p', 'k.ppsu_id=p.id', 'left');
        $this->db->where('k.kelurahan_id', $kelurahan_id);
        $this->db->where('k.kode_id', $kode_id);
        $this->db->where("DATE_FORMAT(k.tanggal, '%Y-%m')='".$tanggal."'");
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec;
        else
            return null;
    }
}