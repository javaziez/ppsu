<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('m_user');
    }
    
    public function index()
    {
        $data = null;

        if($this->input->post('username')!='' && $this->input->post('password')!='')
        {
            $this->form_validation->set_rules('username','Username','trim|required');
            $this->form_validation->set_rules('password','Password','trim|required');
            
            if($this->form_validation->run() == true)
            {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                
                $cek = $this->m_user->cekLogin($username, $password);
                if($cek!=null)
                {
                    $this->session->set_userdata('id', $cek->id);
                    $this->session->set_userdata('username', $username);
                    $this->session->set_userdata('kelurahan_id', $cek->kelurahan_id);
                    $this->session->set_userdata('nama_kel', $cek->nama_kel);
                    $this->session->set_userdata('level_id', $cek->level_id);
                    $this->session->set_userdata('nama_level', $cek->nama_level);
                    $this->session->set_userdata('tamu', ($cek->level_id==4) ? true : false);

                    redirect(base_url().'home');
                }
                else
                {
                    $data['status'] = 'danger';
                    $data['pesan'] = 'Hak akses ditolak!';
                }
            }
            else
            {
                $data['status'] = 'danger';
                $data['pesan'] = 'Inputan salah!';
            }
        }
        
        $data = array(
                    'view'          => 'login',
                    'judul'         => 'Login',
                    'css'           => array(),
        );
        
        $this->load->view('login', $data);
    }
    
    public function logout()
    {
        session_destroy();
        redirect(base_url().'login');
    }
}
