<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penilaian extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')>2)
            redirect('login');
        
        $this->load->model('m_penilaian');
    }
    
    public function index()
    {
        $rec = $this->m_penilaian->listPenilaian();
        
        $data = array(
                    'view'          => 'penilaian/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Penilaian',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function upload()
    {
        if($this->input->post('kelurahan_id')!=null)
        {
            $kelurahan_id = $this->input->post('kelurahan_id');
            $tahun = $this->input->post('tahun');
            $bulan = $this->input->post('bulan');

            $rec = $this->m_penilaian->filterAbsensi($kelurahan_id, $tahun, $bulan);
        }
        else
        {
            $kelurahan_id = null;
            $tahun = null;
            $bulan = null;
            $rec = null;
        }
        
        $data = array(
                    'view'          => 'penilaian/upload',
                    'judul'         => 'Penilaian',
                    'js'            => array('validation'=>'validation','select2'=>'select2','datatables'=>'datatables'),
                    'css'           => array('select2'=>'select2','datatables'=>'datatables'),
                    'cmbKelurahan'  => $this->m_penilaian->cmbKelurahan(),
                    'kelurahan'     => $this->m_penilaian->getNamaKelurahan($this->session->userdata('kelurahan_id')),
                    'kelurahan_id'  => $kelurahan_id,
                    'tahun'         => $tahun,
                    'bulan'         => $bulan,
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    function proses_download()
    {
        $kelurahan_id = $this->input->post('kelurahan_id');
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $nama_bulan = $bulan;
        
        $bln = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $bulan = array_search($bulan, $bln) + 1;
        
        $rec = $this->m_penilaian->getAbsensi($kelurahan_id, $tahun, $bulan);
        
        $data = array();
        if($rec!=null)
        {
            $dt = $rec->row();

            $this->load->helper('writer');

            // excel writer
            $workbook = new Spreadsheet_Excel_Writer();
            $workbook->send('Absensi_'.$dt->nama_kel.'_'.$tahun.'_'.$nama_bulan.'.xls');

            $worksheet =& $workbook->addWorksheet("Absensi");
            $worksheet->write(0,0,"FORM NILAI");
            $worksheet->write(1,0,"Kelurahan");
            $worksheet->write(1,1,$dt->nama_kel);
            $worksheet->write(2,0,"Tahun");
            $worksheet->write(2,1,$tahun);
            $worksheet->write(3,0,"Bulan");
            $worksheet->write(3,1,$nama_bulan);

            $header = array("No.", "Nama PPSU", "Alpha", "Sakit", "Izin", "Disiplin Kehadiran", "Tanggung Jawab Penyelesaian Pekerjaan", "Kepatuhan terhadap Kewajiban dan Larangan", "Keterangan");

            for($i=0;$i<9;$i++)
                $worksheet->write(5,$i,$header[$i]);

            $i=1;
            $noBarisCell = 6;
            foreach($rec->result() as $r)
            {
                $worksheet->write($noBarisCell,0,$i);
                $worksheet->write($noBarisCell,1,$r->nama);
                $worksheet->write($noBarisCell,2,$r->alpha);
                $worksheet->write($noBarisCell,3,$r->sakit);
                $worksheet->write($noBarisCell,4,$r->izin);
                $worksheet->write($noBarisCell,5,$r->nilai_1);
                $worksheet->write($noBarisCell,6,$r->nilai_2);
                $worksheet->write($noBarisCell,7,$r->nilai_3);
                $worksheet->write($noBarisCell,8,$r->keterangan);

                $i++;
                $noBarisCell++;
            }

            $workbook->close();
        }
        else
        {
            $this->session->set_userdata('status', 'warning');
            $this->session->set_userdata('pesan', 'Data PPSU Kosong');

            redirect(base_url().'penilaian/upload');
        }
    }
    
    public function proses_upload()
    {
        // insert file
        $config['upload_path']          = './temp/';
        $config['allowed_types']        = 'xls|xlsx|application/cdfv2-corrupt';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('fileupload'))
        {
            $data = array('upload_data'=>$this->upload->data());

            if(file_exists('temp/'.$this->upload->data('file_name')))
            {
                $namafile='temp/'.$this->upload->data('file_name');
                
                $this->load->helper('reader');
                
                $data=new Spreadsheet_Excel_Reader(); //instansiasi class
                $data->setOutputEncoding('CP1251'); //menentukan encoding
                
                $data->read($namafile); //baca file
                
                $kelurahan = $data->sheets[0]['cells'][2][2];
                
                //cek akses data
                if($this->session->userdata('level_id')==1 || ($this->session->userdata('level_id')!=1 && $this->session->userdata('nama_kel')==$kelurahan) )
                {   
                    $tahun = $data->sheets[0]['cells'][3][2];
                    $bulan = $data->sheets[0]['cells'][4][2];

                    $bln = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                    $bulan = array_search($bulan, $bln) + 1;

                    $exist = $this->m_penilaian->cekExistNilai($kelurahan,$tahun,$bulan);
                    if($exist) // update
                    {
                        for($i=7; $i<=$data->sheets[0]['numRows']; $i++)
                        {
                            $input = array(
                                'alpha'             => (isset($data->sheets[0]['cells'][$i][3])) ? $data->sheets[0]['cells'][$i][3] : '0',
                                'sakit'             => (isset($data->sheets[0]['cells'][$i][4])) ? $data->sheets[0]['cells'][$i][4] : '0',
                                'izin'              => (isset($data->sheets[0]['cells'][$i][5])) ? $data->sheets[0]['cells'][$i][5] : '0',
                                'nilai_1'           => (isset($data->sheets[0]['cells'][$i][6])) ? $data->sheets[0]['cells'][$i][6] : '0',
                                'nilai_2'           => (isset($data->sheets[0]['cells'][$i][7])) ? $data->sheets[0]['cells'][$i][7] : '0',
                                'nilai_3'           => (isset($data->sheets[0]['cells'][$i][8])) ? $data->sheets[0]['cells'][$i][8] : '0',
                                'keterangan'        => (isset($data->sheets[0]['cells'][$i][9])) ? $data->sheets[0]['cells'][$i][9] : '',
                                'updated_date'      => date("Y-m-d H:i:s"),
                                'updated_by'        => $this->session->userdata('id'),
                            );
                            $this->m_penilaian->updatePenilaian($input, $kelurahan, $tahun, $bulan, $data->sheets[0]['cells'][$i][2]);
                        }
                    }
                    else
                    {
                        for($i=7; $i<=$data->sheets[0]['numRows']; $i++)
                        {
                            $ppsu_id = $this->m_penilaian->getIdPpsu($data->sheets[0]['cells'][$i][2]);

                            if($ppsu_id!=null)
                            {
                                $input = array(
                                    'ppsu_id'           => $ppsu_id,
                                    'waktu'             => date("Y-m-d", strtotime($tahun.'-'.$bulan.'-01')),
                                    'alpha'             => (isset($data->sheets[0]['cells'][$i][3])) ? $data->sheets[0]['cells'][$i][3] : '0',
                                    'sakit'             => (isset($data->sheets[0]['cells'][$i][4])) ? $data->sheets[0]['cells'][$i][4] : '0',
                                    'izin'              => (isset($data->sheets[0]['cells'][$i][5])) ? $data->sheets[0]['cells'][$i][5] : '0',
                                    'nilai_1'           => (isset($data->sheets[0]['cells'][$i][6])) ? $data->sheets[0]['cells'][$i][6] : '0',
                                    'nilai_2'           => (isset($data->sheets[0]['cells'][$i][7])) ? $data->sheets[0]['cells'][$i][7] : '0',
                                    'nilai_3'           => (isset($data->sheets[0]['cells'][$i][8])) ? $data->sheets[0]['cells'][$i][8] : '0',
                                    'keterangan'        => (isset($data->sheets[0]['cells'][$i][9])) ? $data->sheets[0]['cells'][$i][9] : '',
                                    'created_date'      => date("Y-m-d H:i:s"),
                                    'created_by'        => $this->session->userdata('id'),
                                );
                                $this->m_penilaian->insertPenilaian($input);
                            }
                        }
                    }

                    $this->session->set_userdata('status', 'success');
                    $this->session->set_userdata('pesan', "Data Penilaian Berhasil Disimpan");
                }
                else
                {
                    $this->session->set_userdata('status', 'danger');
                    $this->session->set_userdata('pesan', "Anda Tidak Berhak Mengakses Data Ini");
                }
                
                unlink($namafile);

                redirect(base_url().'penilaian/upload');
            }
            else
            {
                $this->session->set_userdata('status', 'warning');
                $this->session->set_userdata('pesan', "File Upload Tidak Terupload");

                redirect(base_url().'penilaian/upload');
            }
        }
        else
        {
            $data = array('upload_data'=>$this->upload->data());
            
            $this->session->set_userdata('status', 'warning');
            $this->session->set_userdata('pesan', $this->upload->display_errors()."Tipe File Anda : ".$data['upload_data']['file_type']."");

            redirect(base_url().'penilaian/upload');
        }
    }
    
    public function view($kel_id, $waktu)
    {
        $kel_id = base64_decode(urldecode($kel_id));
        $waktu = base64_decode(urldecode($waktu));
        
        $rec = $this->m_penilaian->viewPenilaian($kel_id, $waktu);
                
        $data = array(
                    'view'          => 'penilaian/view',
                    'judul'         => 'Penilaian',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'rec'           => $rec,
                    'bulan'         => ($rec!=null) ? $rec->row()->bulan : '',
                    'kelurahan'     => ($rec!=null) ? $rec->row()->nama_kel: '',
                    'kel_id'        => $kel_id,
                    'waktu'         => $waktu,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create($kel_id, $waktu)
    {
        $kelurahan_id = base64_decode(urldecode($kel_id));
        $date = base64_decode(urldecode($waktu));
        
        $nama_kel = $this->m_penilaian->getNamaKelurahan($kelurahan_id);
        $bulan = date('F Y', strtotime($date));
        
        if($this->input->post('ppsu_id')!=null)
        {
            $input = array(
                'ppsu_id'           => $this->input->post('ppsu_id'),
                'waktu'             => $date,
                'alpha'             => $this->input->post('alpha'),
                'sakit'             => $this->input->post('sakit'),
                'izin'              => $this->input->post('izin'),
                'nilai_1'           => $this->input->post('nilai_1'),
                'nilai_2'           => $this->input->post('nilai_2'),
                'nilai_3'           => $this->input->post('nilai_3'),
                'keterangan'        => $this->input->post('keterangan'),
                'created_date'      => date("Y-m-d H:i:s"),
                'created_by'        => $this->session->userdata('id'),
            );
            $this->m_penilaian->insertPenilaian($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Detail Data Penilaian berhasil ditambah');

            redirect(base_url().'penilaian/view/'.$kel_id.'/'.$waktu);
        }
        
        $data = array(
                    'view'          => 'penilaian/create',
                    'judul'         => 'Penilaian',
                    'js'            => array('select2'=>'select2','validation'=>'validation'),
                    'css'           => array('select2'=>'select2'),
                    'nama_kel'      => $nama_kel,
                    'bulan'         => $bulan,
                    'cmbPpsu'       => $this->m_penilaian->cmbPpsu($kelurahan_id),
                    'url'           => $kel_id.'/'.$waktu,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($kel_id, $waktu, $id)
    {
        $rec = $this->m_penilaian->detailPenilaian($id);
        
        if($this->input->post('alpha')!=null)
        {
            $input = array(
                'alpha'             => $this->input->post('alpha'),
                'sakit'             => $this->input->post('sakit'),
                'izin'              => $this->input->post('izin'),
                'nilai_1'           => $this->input->post('nilai_1'),
                'nilai_2'           => $this->input->post('nilai_2'),
                'nilai_3'           => $this->input->post('nilai_3'),
                'keterangan'        => $this->input->post('keterangan'),
                'updated_date'      => date("Y-m-d H:i:s"),
                'updated_by'        => $this->session->userdata('id'),
            );
            $this->m_penilaian->updateDetail($input, $id);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Detail Data Penilaian berhasil diubah');

            redirect(base_url().'penilaian/view/'.$kel_id.'/'.$waktu);
        }
        
        $data = array(
                    'view'          => 'penilaian/edit',
                    'judul'         => 'Penilaian',
                    'js'            => array('validation'=>'validation'),
                    'css'           => array(),
                    'rec'           => $rec,
                    'url'           => $kel_id.'/'.$waktu.'/'.$id,
        );
        
        $this->load->view('template', $data);
    }
    
    public function delete($kel_id, $waktu)
    {
        if($this->input->post('delete_id')!=null)
        {
            $this->m_penilaian->deleteDetail($this->input->post('delete_id'));
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Detail Data Penilaian berhasil dihapus');

            redirect(base_url().'penilaian/view/'.$kel_id.'/'.$waktu);
        }
    }
}