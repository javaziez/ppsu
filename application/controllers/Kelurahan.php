<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')>2)
            redirect('login');
        
        $this->load->model('m_kelurahan');
    }
    
    public function index()
    {
        $rec = $this->m_kelurahan->listKelurahan();
        
        $data = array(
                    'view'          => 'kelurahan/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Kelurahan',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->session->userdata('level_id')==1)
        {
            if($this->input->post('nama')!=null)
            {
                $input = array(
                    'nama'          => $this->input->post('nama'),
                    'lurah'         => $this->input->post('lurah'),
                    'nip_lurah'     => $this->input->post('nip_lurah'),
                    'korlap'        => $this->input->post('korlap'),
                    'nip_korlap'    => $this->input->post('nip_korlap'),
                );
                $id = $this->m_kelurahan->insertKelurahan($input);

                $this->session->set_userdata('status', 'info');
                $this->session->set_userdata('pesan', 'Data Kelurahan berhasil disimpan');

                redirect(base_url().'kelurahan/view/'.$id);
            }

            $data = array(
                        'view'          => 'kelurahan/create',
                        'judul'         => 'Kelurahan',
                        'js'            => array('validation'=>'validation'),
                        'css'           => array(),
            );

            $this->load->view('template', $data);
        }
        else
        {
            redirect(base_url().'kelurahan');
        }
    }
    
    public function view($id)
    {
        $rec = $this->m_kelurahan->viewKelurahan($id);
        
        $data = array(
                    'view'          => 'kelurahan/view',
                    'judul'         => 'Kelurahan',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $rec = $this->m_kelurahan->viewKelurahan($id);
        
        if($this->session->userdata('kelurahan_id')==1 || $this->session->userdata('kelurahan_id')==$rec->id)
        {
            if($this->input->post('nama')!=null)
            {
                $input = array(
                    'nama'          => $this->input->post('nama'),
                    'lurah'         => $this->input->post('lurah'),
                    'nip_lurah'     => $this->input->post('nip_lurah'),
                    'korlap'        => $this->input->post('korlap'),
                    'nip_korlap'    => $this->input->post('nip_korlap'),
                );
                $this->m_kelurahan->updateKelurahan($input, $rec->id);

                $this->session->set_userdata('status', 'info');
                $this->session->set_userdata('pesan', 'Data Kelurahan berhasil diubah');

                redirect(base_url().'kelurahan/view/'.$rec->id);
            }

            $data = array(
                        'view'          => 'kelurahan/edit',
                        'judul'         => 'Kelurahan',
                        'js'            => array('validation'=>'validation'),
                        'css'           => array(),
                        'rec'           => $rec,
            );

            $this->load->view('template', $data);
        }
        else
        {
            redirect(base_url().'kelurahan');
        }
    }
}