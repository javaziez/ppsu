<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Monitoring extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null)
            redirect('login');
        
        $this->load->model('m_monitoring');
        $this->load->model('m_kegiatan');
    }
    
    public function index()
    {
        $rec = $this->m_monitoring->listMonitoring();
        
        $data = array(
                    'view'          => 'monitoring/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Monitoring',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function view($id)
    {
        $rec = $this->m_kegiatan->viewKegiatan($id);
        
        $data = array(
                    'view'          => 'monitoring/view',
                    'judul'         => 'Monitoring',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
}