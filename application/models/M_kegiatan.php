<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_kegiatan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listKegiatan()
    {
        $this->db->select('k.id, tanggal_cetak, lokasi, kod.nama as nama_kode, kel.nama as nama_kel, kelurahan_id');
        $this->db->from('kegiatan as k');
        $this->db->join('kode as kod', 'k.kode_id=kod.id', 'left');
        $this->db->join('kelurahan as kel', 'k.kelurahan_id=kel.id', 'left');
        $this->db->order_by('k.id desc');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKelurahan()
    {
        $this->db->select("id, nama");
        $this->db->from('kelurahan');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKetua($kelurahan_id)
    {
        $this->db->select("id, nama");
        $this->db->from('ppsu');
        $this->db->where('kelurahan_id', $kelurahan_id);
        $this->db->where('ketua', true);
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbPpsu()
    {
        $this->db->select("id, nama");
        $this->db->from('ppsu');
        $this->db->where('kelurahan_id', $this->session->userdata('kelurahan_id') );
        $this->db->where('ketua', true);
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKode()
    {
        $this->db->select("*");
        $this->db->from('kode');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbSumber()
    {
        $this->db->select("*");
        $this->db->from('sumber');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertKegiatan($rec)
    {
        $this->db->insert('kegiatan', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewKegiatan($id)
    {
        $this->db->select('k.*, s.nama as nama_sumber, kod.nama as nama_kode, kel.nama as nama_kel, p.nama as nama_ppsu');
        $this->db->from('kegiatan as k');
        $this->db->join('sumber as s', 'k.sumber_id=s.id', 'left');
        $this->db->join('kode as kod', 'k.kode_id=kod.id', 'left');
        $this->db->join('kelurahan as kel', 'k.kelurahan_id=kel.id', 'left');
        $this->db->join('ppsu as p', 'k.ppsu_id=p.id', 'left');
        $this->db->where('k.id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateKegiatan($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('kegiatan', $rec);
    }
}