<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH.'/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

class Laporan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')>2)
            redirect('login');
        
        $this->load->model('m_laporan');
    }
    
    public function index()
    {   
        $data = array(
                    'view'          => 'laporan/index',
                    'js'            => array('select2'=>'select2','datepicker'=>'datepicker'),
                    'css'           => array('select2'=>'select2'),
                    'judul'         => 'Laporan',
                    'cmbKelurahan'  => $this->m_laporan->cmbKelurahan(),
                    'cmbKode'       => $this->m_laporan->cmbKode(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function proses()
    {
        if($this->input->post('cetak')=='excel')
            $this->proses_excel();
        
        if($this->input->post('jenis_laporan')!=null)
        {
            if($this->input->post('jenis_laporan')=='harian')
            {
                $tanggal = date("Y-m-d", strtotime($this->input->post('tanggal')));

                $rec = $this->m_laporan->getLaporanHarian($this->input->post('kelurahan_id'), $this->input->post('kode_id'), $tanggal);
                
                if($rec!=null)
                {
                    ob_start();

                    $data = array(
                                'rec'       => $rec,
                    );
                    $this->load->view('laporan/harian', $data);

                    $content = ob_get_clean();

                    try
                    {
                        $html2pdf = new HTML2PDF('L','Legal','en', true, 'UTF-8',array(15.24, 12.7, 15.24, 7.62));

                        $html2pdf->setDefaultFont('Arial');
                        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                        $html2pdf->Output($rec->row()->lokasi.'.pdf');
                    }
                    catch(HTML2PDF_exception $e) 
                    { 
                        echo $e; 
                    }
                }
                else
                {
                    $this->session->set_userdata('status', 'info');
                    $this->session->set_userdata('pesan', 'Data Tidak Ada');
                    
                    redirect(base_url().'laporan');
                }
            }
            else // bulanan
            {
                $tanggal = date("Y-m", strtotime($this->input->post('tanggal')));

                $rec = $this->m_laporan->getLaporanBulanan($this->input->post('kelurahan_id'), $this->input->post('kode_id'), $tanggal);
                
                if($rec!=null)
                {
                    ob_start();

                    $data = array(
                                'rec'       => $rec,
                    );
                    $this->load->view('laporan/bulanan', $data);

                    $content = ob_get_clean();

                    try
                    {
                        $html2pdf = new HTML2PDF('L','Legal','en', true, 'UTF-8',array(15.24, 12.7, 15.24, 7.62));

                        $html2pdf->setDefaultFont('Arial');
                        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                        $html2pdf->Output($rec->row()->lokasi.'.pdf');
                    }
                    catch(HTML2PDF_exception $e) 
                    { 
                        echo $e; 
                    }
                }
                else
                {
                    $this->session->set_userdata('status', 'info');
                    $this->session->set_userdata('pesan', 'Data Tidak Ada');
                    
                    redirect(base_url().'laporan');
                }
            }
        }
    }
    
    public function proses_excel()
    {
        if($this->input->post('jenis_laporan')!=null)
        {
            if($this->input->post('jenis_laporan')=='harian')
            {
                $tanggal = date("Y-m-d", strtotime($this->input->post('tanggal')));

                $rec = $this->m_laporan->getLaporanHarian($this->input->post('kelurahan_id'), $this->input->post('kode_id'), $tanggal);
                
                if($rec!=null)
                {
                    $data = $rec->row();
                    
                    $this->load->helper('writer');
                    $workbook = new Spreadsheet_Excel_Writer();
                    $workbook->send('LaporanHarian_'.$data->nama_kel.'_'.$data->nama_kode.'.xls');
                    $worksheet =& $workbook->addWorksheet("Laporan Harian");
                    
                    $worksheet->write(0,0,"LAPORAN HARIAN PELAKSANAAN PENANGANAN PRASARANA DAN SARANA UMUM TINGKAT KELURAHAN");
                    $worksheet->write(2,4,"Kode");
                    $worksheet->write(2,5,": ".$data->kode_id.' : '.$data->nama_kode);
                    $worksheet->write(3,0,"Kelurahan");
                    $worksheet->write(3,1,": ".$data->nama_kel);
                    $worksheet->write(4,0,"Hari");
                    $worksheet->write(4,1,": ".$data->hari);
                    $worksheet->write(5,0,"Tanggal");
                    $worksheet->write(5,1,": ".$data->tanggal_cetak);
                    
                    $header = array("NO.", "SUMBER INFORMASI", "KONDISI LAPANGAN", "PENANGANAN (PEKERJAAN PPSU TINGKAT KELURAHAN)", "LOKASI", "PROGRES PEKERJAAN", "KETERANGAN");
                    
                    for($i=0;$i<7;$i++)
                        $worksheet->write(7,$i,$header[$i]);
                    
                    for($i=0;$i<7;$i++)
                        $worksheet->write(8,$i,($i+1));
                    
                    $i=1;
                    $noBarisCell = 9;
                    foreach($rec->result() as $r)
                    {
                        $worksheet->write($noBarisCell,0,$i);
                        $worksheet->write($noBarisCell,1,$r->nama_sumber);
                        $worksheet->write($noBarisCell,2,$r->kondisi);
                        $worksheet->write($noBarisCell,3,$r->pekerjaan);
                        $worksheet->write($noBarisCell,4,$r->lokasi);
                        $worksheet->write($noBarisCell,5,$r->progress);
                        $worksheet->write($noBarisCell,6,$r->keterangan);
                        
                        $i++;
                        $noBarisCell++;
                    }
                    
                    $worksheet->write(($noBarisCell+1),1,"Koordinator Lapangan");
                    $worksheet->write(($noBarisCell+1),5,"Ketua Kelompok");
                    
                    $worksheet->write(($noBarisCell+5),1,"(".$data->korlap.")");
                    $worksheet->write(($noBarisCell+5),5, $data->nama_ppsu);
                    
                    $worksheet->write(($noBarisCell+6),1,'NIP.'.$data->nip_korlap);
                    
                    $worksheet->write(($noBarisCell+7),3,"Mengetahui,");
                    $worksheet->write(($noBarisCell+8),3, 'Lurah '.$data->nama_kel);
                    
                    $worksheet->write(($noBarisCell+12),3,"(".$data->lurah.")");
                    $worksheet->write(($noBarisCell+13),3, 'NIP. '.$data->nip_lurah);
                    
                    $worksheet->write(($noBarisCell+16),0,"1.");
                    $worksheet->write(($noBarisCell+16),1,"Nomor urut.");
                    $worksheet->write(($noBarisCell+17),0,"2.");
                    $worksheet->write(($noBarisCell+17),1,"Sumber Informasi : ");
                    $worksheet->write(($noBarisCell+18),1,"a. Hasil survey lapangan oleh Kelurahan;");
                    $worksheet->write(($noBarisCell+19),1,"b. Laporan secara tertulis dan / atau lisan dari masyarakat setempat;");
                    $worksheet->write(($noBarisCell+20),1,"c. Laporan dari Aplikasi Qlue;");
                    $worksheet->write(($noBarisCell+21),1,"d. Hasil koordinasi dengan Perangkat Daerah Teknis terkait; dan / atau");
                    $worksheet->write(($noBarisCell+22),1,"e. Disposisi dari Pimpinan;");
                    $worksheet->write(($noBarisCell+23),0,"3.");
                    $worksheet->write(($noBarisCell+23),1,"Kondisi Lapangan : Gambaran Kondisi yang dilaporkan.");
                    $worksheet->write(($noBarisCell+24),0,"4.");
                    $worksheet->write(($noBarisCell+24),1,"Penanganan (Pekerjaan PPSU Tingkat Kelurahan) : Pekerjaan / Aktivitas PPSU Tingkat Kelurahan terhadap Kondisi Lapangan.");
                    $worksheet->write(($noBarisCell+25),0,"5.");
                    $worksheet->write(($noBarisCell+25),1,"Lokasi : Obyek dari Jalan, Gang, Kelurahan, Kecamatan, Kota/Kabupaten Administrasi.");
                    $worksheet->write(($noBarisCell+26),0,"6.");
                    $worksheet->write(($noBarisCell+26),1,"Progres Pekerjaan : Progres Pekerjaan Secara Harian.");
                    
                    $workbook->close();
                }
                else
                {
                    $this->session->set_userdata('status', 'info');
                    $this->session->set_userdata('pesan', 'Data Tidak Ada');
                    
                    redirect(base_url().'laporan');
                }
            }
            else // bulanan
            {
                $tanggal = date("Y-m", strtotime($this->input->post('tanggal')));

                $rec = $this->m_laporan->getLaporanBulanan($this->input->post('kelurahan_id'), $this->input->post('kode_id'), $tanggal);
                
                if($rec!=null)
                {
                    $data = $rec->row();
                    
                    $this->load->helper('writer');
                    $workbook = new Spreadsheet_Excel_Writer();
                    $workbook->send('LaporanBulanan_'.$data->nama_kel.'_'.$data->nama_kode.'.xls');
                    $worksheet =& $workbook->addWorksheet("Laporan Bulanan");
                    
                    $worksheet->write(0,0,"LAPORAN BULANAN PELAKSANAAN PENANGANAN PRASARANA DAN SARANA UMUM TINGKAT KELURAHAN");
                    $worksheet->write(2,4,"Kode");
                    $worksheet->write(2,5,": ".$data->kode_id.' : '.$data->nama_kode);
                    $worksheet->write(3,0,"Kelurahan");
                    $worksheet->write(3,1,": ".$data->nama_kel);
                    $worksheet->write(4,0,"Bulan");
                    $worksheet->write(4,1,": ".date('F', strtotime($data->tanggal)));
                    
                    $header = array("NO.", "HARI", "TANGGAL", "PENANGANAN (PEKERJAAN PPSU TINGKAT KELURAHAN)", "LOKASI", "PROGRES PEKERJAAN", "KETERANGAN");
                    
                    for($i=0;$i<7;$i++)
                        $worksheet->write(6,$i,$header[$i]);
                    
                    for($i=0;$i<7;$i++)
                        $worksheet->write(7,$i,($i+1));
                    
                    $i=1;
                    $noBarisCell = 8;
                    foreach($rec->result() as $r)
                    {
                        $worksheet->write($noBarisCell,0,$i);
                        $worksheet->write($noBarisCell,1,$r->hari);
                        $worksheet->write($noBarisCell,2,$r->tanggal_cetak);
                        $worksheet->write($noBarisCell,3,$r->pekerjaan);
                        $worksheet->write($noBarisCell,4,$r->lokasi);
                        $worksheet->write($noBarisCell,5,$r->progress);
                        $worksheet->write($noBarisCell,6,$r->keterangan);
                        
                        $i++;
                        $noBarisCell++;
                    }
                    
                    $worksheet->write(($noBarisCell+1),1,"Koordinator Lapangan");
                    $worksheet->write(($noBarisCell+1),5,"Ketua Kelompok");
                    
                    $worksheet->write(($noBarisCell+5),1,"(".$data->korlap.")");
                    $worksheet->write(($noBarisCell+5),5, $data->nama_ppsu);
                    
                    $worksheet->write(($noBarisCell+6),1,'NIP.'.$data->nip_korlap);
                    
                    $worksheet->write(($noBarisCell+7),3,"Mengetahui,");
                    $worksheet->write(($noBarisCell+8),3, 'Lurah '.$data->nama_kel);
                    
                    $worksheet->write(($noBarisCell+12),3,"(".$data->lurah.")");
                    $worksheet->write(($noBarisCell+13),3, 'NIP. '.$data->nip_lurah);
                    
                    $worksheet->write(($noBarisCell+16),0,"1.");
                    $worksheet->write(($noBarisCell+16),1,"Nomor urut.");
                    $worksheet->write(($noBarisCell+17),0,"2.");
                    $worksheet->write(($noBarisCell+17),1,"Hari.");
                    $worksheet->write(($noBarisCell+18),0,"3.");
                    $worksheet->write(($noBarisCell+18),1,"Tanggal");
                    $worksheet->write(($noBarisCell+19),0,"4.");
                    $worksheet->write(($noBarisCell+19),1,"Penanganan (Pekerjaan PPSU Tingkat Kelurahan) : Pekerjaan / Aktivitas PPSU Tingkat Kelurahan terhadap Kondisi Lapangan.");
                    $worksheet->write(($noBarisCell+20),0,"5.");
                    $worksheet->write(($noBarisCell+20),1,"Lokasi : Obyek dari Jalan, Gang, Kelurahan, Kecamatan, Kota/Kabupaten Administrasi.");
                    $worksheet->write(($noBarisCell+21),0,"6.");
                    $worksheet->write(($noBarisCell+21),1,"Progres Pekerjaan : Progres Pekerjaan Secara Harian.");
                    
                    $workbook->close();
                }
                else
                {
                    $this->session->set_userdata('status', 'info');
                    $this->session->set_userdata('pesan', 'Data Tidak Ada');
                    
                    redirect(base_url().'laporan');
                }
            }
        }
    }
}