<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Kelurahan</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'kelurahan'; ?>">List</button>
                    <?php if($this->session->userdata('kelurahan_id')==1 || $this->session->userdata('kelurahan_id')==$rec->id) { ?>
                        <button type="button" class="btn btn-success" onclick=location.href="<?php echo base_url().'kelurahan/create'; ?>">Tambah</button>
                        <button type="button" class="btn btn-warning" onclick=location.href="<?php echo base_url().'kelurahan/edit/'.$rec->id; ?>">Ubah</button>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if($this->session->userdata('status')!=null) { ?>
                    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
                    </div>
                <?php 
                        $this->session->set_userdata('status', null);
                    }
                ?>
                
                <form class="form-horizontal form-label-left">
                    
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kelurahan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama" value="<?php echo $rec->nama; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lurah">Lurah <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="lurah" value="<?php echo $rec->lurah; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip_lurah">NIP Lurah <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nip_lurah" value="<?php echo $rec->nip_lurah; ?>" />
                    </div>
                </div>
                    <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="korlap">Koordinator Lapangan<span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="korlap" value="<?php echo $rec->korlap; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip_korlap">NIP Koordinator Lapangan<span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nip_korlap" value="<?php echo $rec->nip_korlap; ?>" />
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="button" class="btn btn-primary" onclick=location.href="<?php echo base_url().'kelurahan'; ?>">Kembali</button>
                    </div>
                </div>

                </form>
                
            </div>
        </div>
            
    </div>
</div>