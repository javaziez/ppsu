<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Penilaian PPSU Kelurahan <?php echo $kelurahan; ?> Bulan <?php echo $bulan; ?></h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'penilaian'; ?>">List</button>
                    <button type="button" class="btn btn-warning" onclick=location.href="<?php echo base_url().'penilaian/upload'; ?>">Upload Nilai</button>
                    <button type="button" class="btn btn-success" onclick=location.href="<?php echo base_url().'penilaian/create/'.urlencode(base64_encode($kel_id)).'/'.urlencode(base64_encode($waktu)); ?>">Tambah Penilaian</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if($this->session->userdata('status')!=null) { ?>
                    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
                    </div>
                <?php 
                        $this->session->set_userdata('status', null);
                    }
                ?>
                
                <?php
                if($rec!=null)
                {
                ?>
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>No.</th>
                                <th>Nama PPSU</th>
                                <th>Alpha</th>
                                <th>Sakit</th>
                                <th>Izin</th>
                                <th>Disiplin Kehadiran</th>
                                <th>Tanggung Jawab Penyelesaian Pekerjaan</th>
                                <th>Kepatuhan terhadap Kewajiban dan Larangan</th>
                                <th>Keterangan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                        
                            <?php foreach($rec->result() as $r) {  ?>
                        
                                <tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?> pointer">
                                    <td class="text-center "><?php echo $i; ?></td>
                                    <td class=" "><?php echo $r->nama; ?></td>
                                    <td class="text-center "><?php echo $r->alpha; ?></td>
                                    <td class="text-center "><?php echo $r->sakit; ?></td>
                                    <td class="text-center "><?php echo $r->izin; ?></td>
                                    <td class="text-center "><?php echo $r->nilai_1; ?></td>
                                    <td class="text-center "><?php echo $r->nilai_2; ?></td>
                                    <td class="text-center "><?php echo $r->nilai_3; ?></td>
                                    <td class="text-center "><?php echo $r->keterangan; ?></td>
                                    <td class="a-center last">
                                        <a href="<?php echo base_url().'penilaian/edit/'.urlencode(base64_encode($kel_id)).'/'.urlencode(base64_encode($waktu)).'/'.$r->id; ?>"><span class="fa fa-pencil"></span></a> | 
                                        <a href="#" onclick="modalDelete(<?php echo $r->id ?>,'<?php echo $r->nama; ?>');"><span class="fa fa-trash-o"></span></a>
                                    </td>
                                </tr>
                                
                                <?php $i++; ?>
                            
                            <?php } ?>
                                
                        </tbody>
                    </table>
                <?php
                } else {
                ?>
                        <div class="jumbotron text-center">
                            <h3>DATA KOSONG</h3>
                        </div>
                <?php
                }
                ?>
                
            </div>
        </div>
            
    </div>
</div>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Hapus Detail Data Penilaian</h2>
            </div>
            <div class="modal-body">
                
                Apakah Anda yakin akan menghapus data dengan nama PPSU : <span id="nama_ppsu">&nbsp;</span> ?
                
            </div>
            <div class="modal-footer modal-col-orange">
                
                <form method="post" action="<?php echo base_url().'penilaian/delete/'.urlencode(base64_encode($kel_id)).'/'.urlencode(base64_encode($waktu)); ?>">
                    <input type="hidden" name="delete_id" id="delete_id" value="" />
                    <button id="btn_delete" class="btn btn-success waves-effect pull-right" type="submit">
                        Ya 
                    </button>
                    <button id="btn_batal" class="btn btn-default waves-effect" data-dismiss="modal" type="button">
                        Batal
                    </button>
                </form>
                
            </div>
        </div>
    </div>
</div>

<script>
    function modalDelete(id, nama)
    {
        document.getElementById('delete_id').value = id;
        document.getElementById('nama_ppsu').innerHTML = nama;
        
        $("#modalDelete").modal({backdrop: 'static', keyboard: false});
    }
</script>