<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_monitoring extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listMonitoring()
    {
        $this->db->select('k.id, tanggal_cetak, lokasi, kel.nama as nama_kel, foto_1, foto_2, foto_3, tanggal_cetak, keterangan');
        $this->db->from('kegiatan as k');
        $this->db->join('kelurahan as kel', 'k.kelurahan_id=kel.id', 'left');
        $this->db->order_by('k.id desc');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
}