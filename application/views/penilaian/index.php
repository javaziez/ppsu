<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>List Data Penilaian</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-warning" onclick=location.href="<?php echo base_url().'penilaian/upload'; ?>">Upload Nilai</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>No.</th>
                            <th>Kelurahan</th>
                            <th>Bulan</th>
                            <th width="18%" class=" no-link last"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($rec!=null) {  ?>
                        
                            <?php $i = 1; ?>
                        
                            <?php foreach($rec as $r) {  ?>
                        
                                <tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?> pointer">
                                    <td class="a-center "><?php echo $i; ?></td>
                                    <td class=" "><?php echo $r->nama_kel; ?></td>
                                    <td class=" "><?php echo $r->bulan; ?></td>
                                    <td class="a-center last">
                                        <a class="btn btn-small btn-info" href="<?php echo base_url().'penilaian/view/'.urlencode(base64_encode($r->id)).'/'.urlencode(base64_encode($r->waktu)); ?>">Detail</a>
                                    </td>
                                </tr>
                                
                                <?php $i++; ?>
                            
                            <?php } ?>
                                
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
            
    </div>
</div>