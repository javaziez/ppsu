<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Kegiatan</h2>
                <div class="pull-right">
                    <?php if($this->session->userdata('tamu')==false) { ?>
                        <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'kegiatan'; ?>">List</button>
                        <button type="button" class="btn btn-success" onclick=location.href="<?php echo base_url().'kegiatan/create'; ?>">Tambah</button>
                    <?php } ?>
                    
                    <?php if($this->session->userdata('tamu')==false && $this->session->userdata('level_id')<=2 && ($this->session->userdata('level_id')==1 || $this->session->userdata('kelurahan_id')==$rec->kelurahan_id)) { ?>
                        <button type="button" class="btn btn-warning" onclick=location.href="<?php echo base_url().'kegiatan/edit/'.$rec->id; ?>">Ubah</button>
                    <?php } ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if($this->session->userdata('status')!=null) { ?>
                    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
                    </div>
                <?php 
                        $this->session->set_userdata('status', null);
                    }
                ?>
                
                <form class="form-horizontal form-label-left">
                    
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_kode">Kode <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama_kode" value="<?php echo $rec->nama_kode; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_sumber">Sumber Informasi <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama_sumber" value="<?php echo $rec->nama_sumber; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal">Tanggal <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="tanggal" value="<?php echo date("d F Y", strtotime($rec->tanggal)); ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_kel">Kelurahan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama_kel" value="<?php echo $rec->nama_kel; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_ppsu">Ketua Kelompok <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama_ppsu" value="<?php echo $rec->nama_ppsu; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lokasi">Lokasi <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="lokasi" value="<?php echo $rec->lokasi; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kondisi">Kondisi Lapangan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="kondisi" value="<?php echo $rec->kondisi; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pekerjaan">Penanganan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="pekerjaan" value="<?php echo $rec->pekerjaan; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="progress">Progress Pekerjaan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="progress" value="<?php echo $rec->progress; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="keterangan" value="<?php echo $rec->keterangan; ?>" />
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_1">Foto 0% <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php if($rec->foto_1!=null) { ?>
                            <img src="<?php echo base_url().'files/'.$rec->foto_1; ?>" style="width: 500px;height: 270px;" />
                            <!--<a href="<?php echo base_url().'files/'.$rec->foto_1; ?>" target="_blank">Tersedia</a>-->
                        <?php } else { ?>
                            Tidak Tersedia
                        <?php } ?>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_2">Foto 50% <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php if($rec->foto_2!=null) { ?>
                        <img src="<?php echo base_url().'files/'.$rec->foto_2; ?>" style="width: 500px;height: 270px;" />
                            <!--<a href="<?php echo base_url().'files/'.$rec->foto_2; ?>" target="_blank">Tersedia</a>-->
                        <?php } else { ?>
                            Tidak Tersedia
                        <?php } ?>
                    </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_3">Foto 100% <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php if($rec->foto_3!=null) { ?>
                            <img src="<?php echo base_url().'files/'.$rec->foto_3; ?>" style="width: 500px;height: 270px;" />
                            <!--<a href="<?php echo base_url().'files/'.$rec->foto_3; ?>" target="_blank">Tersedia</a>-->
                        <?php } else { ?>
                            Tidak Tersedia
                        <?php } ?>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="button" class="btn btn-primary" onclick=location.href="<?php echo base_url().'kegiatan'; ?>">Kembali</button>
                    </div>
                </div>

                </form>
                
            </div>
        </div>
            
    </div>
</div>