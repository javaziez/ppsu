<?php
    $data = $rec->row();
?>

<page>
    
    <h5 style="text-align: center; margin-bottom: 0px; font-size: 14px;">
        LAPORAN HARIAN PELAKSANAAN PENANGANAN PRASARANA DAN SARANA UMUM TINGKAT KELURAHAN
    </h5>
    <br />
    
    <table style="width:100%; margin-bottom: 0px; font-size: 12px;">
        <tr>
            <td colspan="2" style="width:50%;">&nbsp;</td>
            <td style="width:50%;">
                Kode : <?php echo $data->kode_id.' : '.$data->nama_kode; ?>
            </td>
        </tr>
        <tr>
            <td style="width:10%;">Kelurahan</td>
            <td style="width:20%;">: <?php echo $data->nama_kel; ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width:10%;">Hari</td>
            <td style="width:20%;">: <?php echo $data->hari; ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width:10%;">Tanggal</td>
            <td style="width:20%;">: <?php echo $data->tanggal_cetak; ?></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    
    <table style="width: 100%; font-size: 11px;"  cellpadding="5" cellspacing="0" border='0.5px'>
        <tr>
            <th style="width: 5%; text-align: center;">NO.</th>
            <th style="width: 15%; text-align: center;">SUMBER INFORMASI</th>
            <th style="width: 15%; text-align: center;">KONDISI LAPANGAN</th>
            <th style="width: 20%; text-align: center;">PENANGANAN <br />(PEKERJAAN PPSU TINGKAT KELURAHAN)</th>
            <th style="width: 15%; text-align: center;">LOKASI</th>
            <th style="width: 15%; text-align: center;">PROGRES PEKERJAAN</th>
            <th style="width: 15%; text-align: center;">KETERANGAN</th>
        </tr>
        <tr>
            <th style="text-align: center;">1</th>
            <th style="text-align: center;">2</th>
            <th style="text-align: center;">3</th>
            <th style="text-align: center;">4</th>
            <th style="text-align: center;">5</th>
            <th style="text-align: center;">6</th>
            <th style="text-align: center;">7</th>
        </tr>
        <?php
            $i = 1;
            foreach($rec->result() as $r)
            {
        ?>
        <tr>
            <td style="text-align: center;"><?php echo $i; ?></td>
            <td><?php echo wordwrap($r->nama_sumber, 30, '<br />'); ?></td>
            <td><?php echo wordwrap($r->kondisi, 30, '<br />'); ?></td>
            <td><?php echo wordwrap($r->pekerjaan, 30, '<br />'); ?></td>
            <td><?php echo wordwrap($r->lokasi, 30, '<br />'); ?></td>
            <td><?php echo wordwrap($r->progress, 20, '<br />'); ?></td>
            <td><?php echo wordwrap($r->keterangan, 30, '<br />'); ?></td>
        </tr>
        <?php
                $i++;
            }
        ?>
    </table>
    <br /><br />
       
    <table style="width:100%; font-size: 12px;"  cellpadding="5" cellspacing="0" border='0px'>
        <tr>
            <td style="width: 10%">&nbsp;</td>
            <td style="width: 25%; text-align: center;">Koordinator Lapangan</td>
            <td style="width: 30%; text-align: center;">&nbsp;</td>
            <td style="width: 25%; text-align: center;">Ketua Kelompok</td>
            <td style="width: 10%">&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align: center;">( <?php echo $data->korlap; ?> )</td>
            <td>&nbsp;</td>
            <td style="text-align: center;"><?php echo $data->nama_ppsu; ?></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align: center;">NIP. <?php echo $data->nip_korlap; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: center;">Mengetahui,</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: center;">Lurah <?php echo $data->nama_kel; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: center;">( <?php echo $data->lurah; ?> )</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: center;">NIP. <?php echo $data->nip_lurah; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    
    <p>
    1. Nomor urut.<br />
    2. Sumber Informasi : <br />
    &nbsp;&nbsp;&nbsp;&nbsp;a. Hasil survey lapangan oleh Kelurahan;<br />
    &nbsp;&nbsp;&nbsp;&nbsp;b. Laporan secara tertulis dan / atau lisan dari masyarakat setempat;<br />
    &nbsp;&nbsp;&nbsp;&nbsp;c. Laporan dari Aplikasi Qlue;<br />
    &nbsp;&nbsp;&nbsp;&nbsp;d. Hasil koordinasi dengan Perangkat Daerah Teknis terkait; dan / atau<br />
    &nbsp;&nbsp;&nbsp;&nbsp;e. Disposisi dari Pimpinan;<br />
    3. Kondisi Lapangan : Gambaran Kondisi yang dilaporkan.<br />
    4. Penanganan (Pekerjaan PPSU Tingkat Kelurahan) : Pekerjaan / Aktivitas PPSU Tingkat Kelurahan terhadap Kondisi Lapangan.<br />
    5. Lokasi : Obyek dari Jalan, Gang, Kelurahan, Kecamatan, Kota/Kabupaten Administrasi.<br />
    6. Progres Pekerjaan : Progres Pekerjaan Secara Harian.
    </p>
    
</page>