<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
//        if($this->session->userdata('id')==null)
//            redirect('login');
        
//        $this->load->model('m_naskahdinas');
    }
    
    public function index()
    {
        $data = array(
                    'view'          => 'home',
//                    'js'            => array('datatable'=>'datatable'),
//                    'css'           => array('datatable'=>'datatable'),
                    'judul'         => 'Home',
        );
        
        $this->load->view('template', $data);
    }
}
