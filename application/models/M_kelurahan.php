<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_kelurahan extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listKelurahan()
    {
        $this->db->select('*');
        $this->db->from('kelurahan');
        if($this->session->userdata('level_id')!=1)
            $this->db->where('id', $this->session->userdata('kelurahan_id'));
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertKelurahan($rec)
    {
        $this->db->insert('kelurahan', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewKelurahan($id)
    {
        $this->db->select('*');
        $this->db->from('kelurahan');
        $this->db->where('id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateKelurahan($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('kelurahan', $rec);
    }
}