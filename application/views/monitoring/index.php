<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Monitoring Kegiatan</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>Kegiatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($rec!=null) {  ?>
                        
                            <?php $i = 1; ?>
                        
                            <?php foreach($rec as $r) {  ?>
                        
                                <tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?> pointer">
                                    <td class=" ">
                                        
                                        <div class="col-sm-4 col-xs-4">
                                            
                                            <div id="carousel-example-generic_<?php echo $i; ?>" class="carousel slide" data-ride="carousel">
                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                    <li data-target="#carousel-example-generic_<?php echo $i; ?>" data-slide-to="0" class="active"></li>
                                                    <li data-target="#carousel-example-generic_<?php echo $i; ?>" data-slide-to="1"></li>
                                                    <li data-target="#carousel-example-generic_<?php echo $i; ?>" data-slide-to="2"></li>
                                                </ol>
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active">
                                                        <img src="<?php echo ($r->foto_1!=null) ? base_url().'files/'.$r->foto_1 : base_url().'files/no_picture.jpg'; ?>" width="320px" height="250px" />
                                                        <div class="carousel-caption">
                                                            <p>Foto 0%</p>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo ($r->foto_2!=null) ? base_url().'files/'.$r->foto_2 : base_url().'files/no_picture.jpg'; ?>" width="320px" height="250px" />
                                                        <div class="carousel-caption">
                                                            <p>Foto 50%</p>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <img src="<?php echo ($r->foto_3!=null) ? base_url().'files/'.$r->foto_3 : base_url().'files/no_picture.jpg'; ?>" width="320px" height="250px" />
                                                        <div class="carousel-caption">
                                                            <p>Foto 100%</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Controls -->
                                                <a class="left carousel-control" href="#carousel-example-generic_<?php echo $i; ?>" role="button" data-slide="prev">
                                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="right carousel-control" href="#carousel-example-generic_<?php echo $i; ?>" role="button" data-slide="next">
                                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>

                                        </div>
                                        
                                        <div class="col-sm-8 col-xs-8">
                                            <div class="jumbotron" style="padding-top: 20px;">
                                                <h3><?php echo $r->lokasi; ?></h3>
                                                <b><?php echo $r->keterangan; ?></b><br />
                                                <?php echo $r->tanggal_cetak; ?><br />
                                                <u><a href="<?php echo base_url().'monitoring/view/'.$r->id; ?>" target="_blank">Lihat Detail</a></u>
                                            </div>
                                        </div>
                                        
                                    </td>
                                </tr>
                                
                                <?php $i++; ?>
                            
                            <?php } ?>
                                
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
            
    </div>
</div>