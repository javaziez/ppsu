<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>PPSU ONLINE KECAMATAN KOJA</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(); ?>foto/favicon.ico" type="image/x-icon">
    
    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url().'assets/gentelella/'; ?>css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url().'assets/gentelella/'; ?>fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/gentelella/'; ?>css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url().'assets/gentelella/'; ?>css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url().'assets/gentelella/'; ?>css/icheck/flat/green.css" rel="stylesheet" />

    <?php if(isset($css['datatables'])) { ?>
    <link href="css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <?php } ?>
    
    <?php if(isset($css['select2'])) { ?>
    <!-- select2 -->
    <link href="<?php echo base_url().'assets/gentelella/'; ?>css/select/select2.min.css" rel="stylesheet">
    <?php } ?>
    
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="<?php echo base_url(); ?>" class="site_title"> <span>PPSU Online</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="<?php echo base_url().'assets/gentelella/'; ?>images/usr.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Selamat datang,</span>
                            <h2><?php echo $this->session->userdata('username'); ?></h2><br />
                        </div>
                    </div>
                    <div class="profile">
                        <h5 style="color:white;">Kelurahan <?php echo $this->session->userdata('nama_kel'); ?> </h5>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>MENU</h3>
                            <ul class="nav side-menu">
                                <li><a href="<?php echo base_url().'home'; ?>"><i class="fa fa-dashboard"></i> Home </a></li>
                                <?php if($this->session->userdata('level_id')==1) { ?>
                                    <li><a href="<?php echo base_url().'user'; ?>"><i class="fa fa-users"></i> User </a></li>
                                    <li><a href="<?php echo base_url().'sumber'; ?>"><i class="fa fa-info"></i> Sumber Informasi </a></li>
                                    <li><a href="<?php echo base_url().'kode'; ?>"><i class="fa fa-list-ul"></i> Kode Kegiatan </a></li>
                                <?php } ?>
                                
                                <?php if($this->session->userdata('level_id')==1 || $this->session->userdata('level_id')==2) { ?>
                                    <li><a href="<?php echo base_url().'kelurahan'; ?>"><i class="fa fa-home"></i> Kelurahan </a></li>
                                <?php } ?>
                                
                                <li><a href="<?php echo base_url().'kegiatan'; ?>"><i class="fa fa-edit"></i> Kegiatan </a></li>
                                
                                <?php if($this->session->userdata('level_id')==4) { ?>
                                    <li><a href="<?php echo base_url().'ppsu'; ?>"><i class="fa fa-user"></i> PPSU </a></li>
                                <?php } ?>
                                
                                <?php if($this->session->userdata('level_id')==1 || $this->session->userdata('level_id')==2) { ?>
                                    <li><a href="<?php echo base_url().'ppsu'; ?>"><i class="fa fa-user"></i> PPSU </a></li>
                                    <li><a href="<?php echo base_url().'penilaian'; ?>"><i class="fa fa-check"></i> Penilaian </a></li>
                                <?php } ?>
                                    
                                <li><a href="<?php echo base_url().'monitoring'; ?>"><i class="fa fa-desktop"></i> Monitoring </a></li>
                                
                                <?php if($this->session->userdata('level_id')==1 || $this->session->userdata('level_id')==2) { ?>
                                    <li><a href="<?php echo base_url().'laporan'; ?>"><i class="fa fa-file"></i> Laporan </a></li>
                                <?php } ?>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                    
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <span class="pull-left" style="margin-left: -30px"><img src="<?php echo base_url().'foto/logo.png'; ?>" width="200px" height="30px" style="margin-top: 13px;"/></span>
                        
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="<?php echo base_url().'assets/gentelella/'; ?>images/usr.png" alt=""><?php echo $this->session->userdata('username'); ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <?php if($this->session->userdata('tamu')==false) { ?>
                                    <li><a href="<?php echo base_url().'profile'; ?>">  Profile</a>
                                    </li>
                                    <?php } ?>
                                    <li><a href="<?php echo base_url().'login/logout'; ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">

                <?php $this->load->view($view); ?>
                
                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Copyright &copy; 2017 <a>Kecamatan Koja</a>. |
                            <span class="lead"> PPSU Online</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>
            <!-- /page content -->

        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/icheck/icheck.min.js"></script>
    
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/custom.js"></script>

    <?php if(isset($js['select2'])) { ?>
    <!-- select2 -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/select/select2.full.js"></script>
    <script>
        $(document).ready(function () {
            $(".select2_single").select2({
                placeholder: "-- Pilih --"
            });
        });
    </script>
    <?php } ?>
        
    <?php if(isset($js['validation'])) { ?>
    <!-- form validation -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/validator/validator.js"></script>
    <script>
        // initialize the validator function
        validator.message['date'] = 'not a real date';

        // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
        $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

        $('.multi.required')
            .on('keyup blur', 'input', function () {
                validator.checkField.apply($(this).siblings().last()[0]);
            });

        // bind the validation to the form submit event
        //$('#send').click('submit');//.prop('disabled', true);

        $('form').submit(function (e) {
            e.preventDefault();
            var submit = true;
            // evaluate the form using generic validaing
            if (!validator.checkAll($(this))) {
                submit = false;
            }

            if (submit)
                this.submit();
            return false;
        });

        /* FOR DEMO ONLY */
        $('#vfields').change(function () {
            $('form').toggleClass('mode2');
        }).prop('checked', false);

        $('#alerts').change(function () {
            validator.defaults.alerts = (this.checked) ? false : true;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);
    </script>
    <?php } ?>
    
    <?php if(isset($js['datepicker'])) { ?>
    <!-- daterangepicker -->
    <script type="text/javascript" src="<?php echo base_url().'assets/gentelella/'; ?>js/moment.min2.js"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/gentelella/'; ?>js/datepicker/daterangepicker.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tanggal').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <?php } ?>
    
    <?php if(isset($js['datatables'])) { ?>
    <!-- Datatables -->
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/datatables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url().'assets/gentelella/'; ?>js/datatables/tools/js/dataTables.tableTools.js"></script>
    <script>
        $(document).ready(function () {
            $('input.tableflat').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        });

        var asInitVals = new Array();
        $(document).ready(function () {
            var oTable = $('#example').dataTable({
                "oLanguage": {
                    "sSearch": "Search all columns:"
                },
                "aoColumnDefs": [
                    {
                        'bSortable': false,
                        'aTargets': [0]
                    } //disables sorting for column one
        ],
                'iDisplayLength': 12,
                "sPaginationType": "full_numbers",
//                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "<?php echo base_url('assets/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
                }
            });
            $("tfoot input").keyup(function () {
                /* Filter on the column based on the index of this element's parent <th> */
                oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
            });
            $("tfoot input").each(function (i) {
                asInitVals[i] = this.value;
            });
            $("tfoot input").focus(function () {
                if (this.className == "search_init") {
                    this.className = "";
                    this.value = "";
                }
            });
            $("tfoot input").blur(function (i) {
                if (this.value == "") {
                    this.className = "search_init";
                    this.value = asInitVals[$("tfoot input").index(this)];
                }
            });
        });
    </script>
    <?php } ?>
    
</body>

</html>