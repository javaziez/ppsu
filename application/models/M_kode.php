<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_kode extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listKode()
    {
        $this->db->select('*');
        $this->db->from('kode');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertKode($rec)
    {
        $this->db->insert('kode', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewKode($id)
    {
        $this->db->select('*');
        $this->db->from('kode');
        $this->db->where('id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateKode($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('kode', $rec);
    }
}