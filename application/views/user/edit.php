<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Ubah Data User</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'user'; ?>">List</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'user/edit/'.$rec->id; ?>" enctype="multipart/form-data">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required">
                                <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                    <option value="<?php echo $r->id; ?>" <?php echo ($rec->kelurahan_id==$r->id) ? 'selected' : ''; ?>><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="level_id">Level<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="level_id" name="level_id" required="required">
                                <?php if($cmbLevel!=null) { foreach($cmbLevel as $r) { ?>
                                    <option value="<?php echo $r->id; ?>" <?php echo ($rec->level_id==$r->id) ? 'selected' : ''; ?>><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('level_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="username" name="username" required="required" class="form-control" maxlength="50" value="<?php echo set_value('username', $rec->username); ?>">
                            <?php echo form_error('username', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            *) Jangan diisi jika tidak diubah
                            <input type="password" id="password" name="password" class="form-control" maxlength="50">
                            <?php echo form_error('password', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password2">Konfirmasi Password<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="password" id="password2" name="password2" class="form-control" maxlength="50" data-validate-linked="password">
                            <?php echo form_error('password2', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="aktif">Status<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="aktif" name="aktif" required="required">
                                <option value="1" <?php echo ($rec->aktif==true) ? 'selected' : ''; ?>>Aktif</option>
                                <option value="0" <?php echo ($rec->aktif==false) ? 'selected' : ''; ?>>Non-Aktif</option>
                            </select>
                            <?php echo form_error('aktif', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'user'; ?>">Kembali</button>
                            <input id="btnsimpan" name="btnsimpan" value="Simpan" type="submit" class="btn btn-success" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
            
    </div>
</div>