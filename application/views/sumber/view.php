<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Detail Sumber</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'sumber'; ?>">List</button>
                    <button type="button" class="btn btn-success" onclick=location.href="<?php echo base_url().'sumber/create'; ?>">Tambah</button>
                    <button type="button" class="btn btn-warning" onclick=location.href="<?php echo base_url().'sumber/edit/'.$rec->id; ?>">Ubah</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if($this->session->userdata('status')!=null) { ?>
                    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
                    </div>
                <?php 
                        $this->session->set_userdata('status', null);
                    }
                ?>
                
                <form class="form-horizontal form-label-left">
                    
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sumber <span class="required"></span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" class="form-control" readonly="" id="nama" value="<?php echo $rec->nama; ?>" />
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="button" class="btn btn-primary" onclick=location.href="<?php echo base_url().'sumber'; ?>">Kembali</button>
                    </div>
                </div>

                </form>
                
            </div>
        </div>
            
    </div>
</div>