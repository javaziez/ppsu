<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_penilaian extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listPenilaian()
    {
        $this->db->select('waktu, DATE_FORMAT(waktu, "%M %Y") as bulan, k.id, k.nama as nama_kel');
        $this->db->from('penilaian as n');
        $this->db->join('ppsu as p', 'n.ppsu_id=p.id', 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        if($this->session->userdata('level_id') != 1)
            $this->db->where('k.id', $this->session->userdata('kelurahan_id'));
        $this->db->group_by('waktu, DATE_FORMAT(waktu, "%Y-%m-%d"), bulan, k.id, k.nama');
        $this->db->order_by('DATE_FORMAT(waktu, "%Y-%m-%d")');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKelurahan()
    {
        $this->db->select("id, nama");
        $this->db->from('kelurahan');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function getNamaKelurahan($id)
    {
        $this->db->select("nama");
        $this->db->from('kelurahan');
        $this->db->where('id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row()->nama;
        else
            return null;
    }
    
    function getAbsensi($kelurahan_id, $tahun, $bulan)
    {
        $this->db->select("p.id as ppsu_key, p.nama, k.nama as nama_kel, n.*");
        $this->db->from('ppsu as p');
        $this->db->join('penilaian as n', "p.id=n.ppsu_id and DATE_FORMAT(waktu, '%Y%c') = '".$tahun.$bulan."'", 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        $this->db->where('kelurahan_id', $kelurahan_id);
        $this->db->order_by('p.nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec;
        else
            return null;
    }
    
    function cekExistNilai($kelurahan,$tahun,$bulan)
    {
        $this->db->select("p.id as ppsu_key, p.nama, k.nama as nama_kel, n.*");
        $this->db->from('ppsu as p');
        $this->db->join('penilaian as n', "p.id=n.ppsu_id", 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        $this->db->where('k.nama', $kelurahan);
        $this->db->where("DATE_FORMAT(waktu, '%Y%c') = '".$tahun.$bulan."'");
        $this->db->order_by('p.nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return true;
        else
            return false;
    }
    
    function getIdPpsu($nama)
    {
        $this->db->select("id");
        $this->db->from('ppsu');
        $this->db->where('nama', $nama);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row()->id;
        else
            return null;
    }
    
    function insertPenilaian($rec)
    {
        $this->db->insert('penilaian', $rec);
        
        return $this->db->insert_id();
    }
    
    function updatePenilaian($rec, $kelurahan, $tahun, $bulan, $nama)
    {
        $this->db->where('k.nama', $kelurahan);
        $this->db->where('p.nama', $nama);
        $this->db->update('penilaian as n '
                        . 'left join ppsu as p on p.id=n.ppsu_id '
                        . "left join kelurahan as k on p.kelurahan_id=k.id and DATE_FORMAT(waktu, '%Y%c') = '".$tahun.$bulan."'", $rec);
    }
    
    function filterAbsensi($kelurahan_id, $tahun, $bulan)
    {
        $this->db->select("p.id as ppsu_key, p.nama, k.nama as nama_kel, n.*");
        $this->db->from('ppsu as p');
        $this->db->join('penilaian as n', "p.id=n.ppsu_id", 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        $this->db->where('kelurahan_id', $kelurahan_id);
        $this->db->where("DATE_FORMAT(waktu, '%Y%c') = '".$tahun.$bulan."'");
        $this->db->order_by('p.nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function viewPenilaian($kel_id, $waktu)
    {
        $this->db->select("p.id as ppsu_key, p.nama, k.nama as nama_kel, n.*, DATE_FORMAT(waktu, '%M %Y') as bulan");
        $this->db->from('ppsu as p');
        $this->db->join('penilaian as n', "p.id=n.ppsu_id", 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        $this->db->where('kelurahan_id', $kel_id);
        $this->db->where("waktu", $waktu);
        $this->db->order_by('p.nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec;
        else
            return null;
    }
    
    function detailPenilaian($id)
    {
        $this->db->select("n.*, DATE_FORMAT(waktu, '%M %Y') as bulan, k.nama as nama_kel, p.nama as nama_ppsu");
        $this->db->from('penilaian as n');
        $this->db->join('ppsu as p', 'n.ppsu_id=p.id', 'left');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        $this->db->where('n.id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateDetail($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('penilaian', $rec);
    }
    
    function cmbPpsu($kelurahan_id)
    {
        $this->db->select("id, nama");
        $this->db->from('ppsu');
        $this->db->where('kelurahan_id', $kelurahan_id);
        $this->db->order_by('nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function deleteDetail($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('penilaian');
    }
}