<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_sumber extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listSumber()
    {
        $this->db->select('*');
        $this->db->from('sumber');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertSumber($rec)
    {
        $this->db->insert('sumber', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewSumber($id)
    {
        $this->db->select('*');
        $this->db->from('sumber');
        $this->db->where('id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateSumber($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('sumber', $rec);
    }
}