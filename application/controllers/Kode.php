<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kode extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')!=1)
            redirect('login');
        
        $this->load->model('m_kode');
    }
    
    public function index()
    {
        $rec = $this->m_kode->listKode();
        
        $data = array(
                    'view'          => 'kode/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Kode',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->input->post('nama')!=null)
        {
            $input = array(
                'nama'          => $this->input->post('nama'),
            );
            $id = $this->m_kode->insertKode($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data Kode berhasil disimpan');

            redirect(base_url().'kode/view/'.$id);
        }
        
        $data = array(
                    'view'          => 'kode/create',
                    'judul'         => 'Kode',
                    'js'            => array('validation'=>'validation'),
                    'css'           => array(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function view($id)
    {
        $rec = $this->m_kode->viewKode($id);
        
        $data = array(
                    'view'          => 'kode/view',
                    'judul'         => 'Kode',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $rec = $this->m_kode->viewKode($id);
        
        if($this->input->post('nama')!=null)
        {
            $input = array(
                'nama'          => $this->input->post('nama'),
            );
            $this->m_kode->updateKode($input, $rec->id);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data Kode berhasil diubah');

            redirect(base_url().'kode/view/'.$rec->id);
        }
        
        $data = array(
                    'view'          => 'kode/edit',
                    'judul'         => 'Kode',
                    'js'            => array('validation'=>'validation'),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
}