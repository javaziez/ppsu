<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<?php if($this->session->userdata('status')!=null) { ?>
    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in text-center" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
    </div>
<?php 
        $this->session->set_userdata('status', null);
    }
?>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Download File Format Upload Data Penilaian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <form id="formdownload" class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'penilaian/proses_download'; ?>">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                            if($this->session->userdata('level_id')==1)
                            {
                            ?>
                                <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required">
                                    <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                        <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                    <?php } } ?>
                                </select>
                            <?php
                            }
                            else
                            {
                            ?>
                                <input type="text" id="kelurahan" name="kelurahan" value="<?php echo $kelurahan; ?>" readonly="" />
                                <input type="hidden" id="kelurahan_id" name="kelurahan_id" value="<?php echo $this->session->userdata('kelurahan_id'); ?>" readonly="" />
                            <?php
                            }
                            ?>
                            <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tahun">Tahun <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="tahun" name="tahun" required="required">
                                <?php for($i=date('Y');$i>=2010;$i--) { ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('tahun', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bulan">Bulan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="bulan" name="bulan" required="required">
                                <option value="Januari">Januari</option>
                                <option value="Februari">Februari</option>
                                <option value="Maret">Maret</option>
                                <option value="April">April</option>
                                <option value="Mei">Mei</option>
                                <option value="Juni">Juni</option>
                                <option value="Juli">Juli</option>
                                <option value="Agustus">Agustus</option>
                                <option value="September">September</option>
                                <option value="Oktober">Oktober</option>
                                <option value="November">November</option>
                                <option value="Desember">Desember</option>
                            </select>
                            <?php echo form_error('bulan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'penilaian'; ?>">Kembali</button>
                            <input id="btndownload" name="btndownload" value="Download" type="submit" class="btn btn-info" />
                        </div>
                    </div>
                
                </form>

            </div>
        </div>
            
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Upload Data Penilaian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <div style="background-color: gainsboro; padding: 3px;">
                    <h4>Cara Upload Nilai :</h4>
                    1. Download file format upload data penilaian.<br />
                    2. Isi kolom nilai dengan nilai angka bulat (alpha, izin, sakit) dan angka desimal (nilai lain selain keterangan).<br />
                    3. Upload file hasil input nilai tersebut.<br />
                    <span style="color:red">4. Jika data penilaian sudah ada sebelumnya, maka data akan direplace dengan data upload terbaru.</span>
                </div>
                <br />
                
                <form id="formupload" class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'penilaian/proses_upload'; ?>" enctype="multipart/form-data">
                
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">File Upload <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type='file' id="fileupload" name='fileupload' value='<?PHP echo set_value("fileupload"); ?>' class="form-control" />
                            <?php echo form_error('fileupload', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'penilaian'; ?>">Kembali</button>
                            <input id="btnupload" name="btnupload" value="Upload Nilai" type="submit" class="btn btn-info" />
                        </div>
                    </div>
                    
                </form>
                
            </div>
        </div>
            
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Data Penilaian</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <form id="formdownload" class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'penilaian/upload'; ?>">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                            if($this->session->userdata('level_id')==1)
                            {
                            ?>
                                <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required">
                                    <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                        <option value="<?php echo $r->id; ?>" <?php echo ($kelurahan_id!=null && $kelurahan_id==$r->id) ? 'selected' : '' ;?>><?php echo $r->nama; ?></option>
                                    <?php } } ?>
                                </select>
                            <?php
                            }
                            else
                            {
                            ?>
                                <input type="text" id="kelurahan" name="kelurahan" value="<?php echo $kelurahan; ?>" readonly="" />
                                <input type="hidden" id="kelurahan_id" name="kelurahan_id" value="<?php echo $this->session->userdata('kelurahan_id'); ?>" readonly="" />
                            <?php
                            }
                            ?>
                            <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tahun">Tahun <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="tahun" name="tahun" required="required">
                                <?php for($i=date('Y');$i>=2010;$i--) { ?>
                                    <option value="<?php echo $i; ?>" <?php echo ($tahun!=null && $tahun==$i) ? 'selected' : '' ;?>><?php echo $i; ?></option>
                                <?php } ?>
                            </select>
                            <?php echo form_error('tahun', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bulan">Bulan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="bulan" name="bulan" required="required">
                                <option value="1" <?php echo ($bulan!=null && $bulan=='1') ? 'selected' : '' ;?>>Januari</option>
                                <option value="2" <?php echo ($bulan!=null && $bulan=='2') ? 'selected' : '' ;?>>Februari</option>
                                <option value="3" <?php echo ($bulan!=null && $bulan=='3') ? 'selected' : '' ;?>>Maret</option>
                                <option value="4" <?php echo ($bulan!=null && $bulan=='4') ? 'selected' : '' ;?>>April</option>
                                <option value="5" <?php echo ($bulan!=null && $bulan=='5') ? 'selected' : '' ;?>>Mei</option>
                                <option value="6" <?php echo ($bulan!=null && $bulan=='6') ? 'selected' : '' ;?>>Juni</option>
                                <option value="7" <?php echo ($bulan!=null && $bulan=='7') ? 'selected' : '' ;?>>Juli</option>
                                <option value="8" <?php echo ($bulan!=null && $bulan=='8') ? 'selected' : '' ;?>>Agustus</option>
                                <option value="9" <?php echo ($bulan!=null && $bulan=='9') ? 'selected' : '' ;?>>September</option>
                                <option value="10" <?php echo ($bulan!=null && $bulan=='10') ? 'selected' : '' ;?>>Oktober</option>
                                <option value="11" <?php echo ($bulan!=null && $bulan=='11') ? 'selected' : '' ;?>>November</option>
                                <option value="12" <?php echo ($bulan!=null && $bulan=='12') ? 'selected' : '' ;?>>Desember</option>
                            </select>
                            <?php echo form_error('bulan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    
                    <div class="ln_solid"></div>
                    
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'penilaian'; ?>">Kembali</button>
                            <input id="btntampil" name="btntampil" value="Tampilkan" type="submit" class="btn btn-info" />
                        </div>
                    </div>
                
                </form>

                <?php
                if($kelurahan_id!=null)
                {
                    if($rec!=null)
                    {
                ?>
                    <table id="example" class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th>No.</th>
                                <th>Nama PPSU</th>
                                <th>Alpha</th>
                                <th>Sakit</th>
                                <th>Izin</th>
                                <th>Disiplin Kehadiran</th>
                                <th>Tanggung Jawab Penyelesaian Pekerjaan</th>
                                <th>Kepatuhan terhadap Kewajiban dan Larangan</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1; ?>
                        
                            <?php foreach($rec as $r) {  ?>
                        
                                <tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?> pointer">
                                    <td class="a-center "><?php echo $i; ?></td>
                                    <td class=" "><?php echo $r->nama; ?></td>
                                    <td class=" "><?php echo $r->alpha; ?></td>
                                    <td class=" "><?php echo $r->sakit; ?></td>
                                    <td class=" "><?php echo $r->izin; ?></td>
                                    <td class=" "><?php echo $r->nilai_1; ?></td>
                                    <td class=" "><?php echo $r->nilai_2; ?></td>
                                    <td class=" "><?php echo $r->nilai_3; ?></td>
                                    <td class=" "><?php echo $r->keterangan; ?></td>
                                </tr>
                                
                                <?php $i++; ?>
                            
                            <?php } ?>
                                
                        </tbody>
                    </table>
                <?php
                    } else {
                ?>
                        <div class="jumbotron text-center">
                            <h3>DATA KOSONG</h3>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
            
    </div>
</div>