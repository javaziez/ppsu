<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Input Data Kegiatan</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'kegiatan'; ?>">List</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'kegiatan/create'; ?>" enctype="multipart/form-data">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_id">Kode <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="kode_id" name="kode_id" required="required">
                                <?php if($cmbKode!=null) { foreach($cmbKode as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('kode_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sumber_id">Sumber Informasi <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="sumber_id" name="sumber_id" required="required">
                                <?php if($cmbSumber!=null) { foreach($cmbSumber as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('sumber_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal">Tanggal <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control tanggal has-feedback-left" id="tanggal" name="tanggal" placeholder="Tanggal" aria-describedby="inputSuccess2Status" required="required" value="<?php echo date('m/d/Y'); ?>">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                    <?php if($this->session->userdata('level_id') == 1) { ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required" onchange="getKetua(this.value)">
                                <option value="">-- Pilih --</option>
                                <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ppsu_id">Ketua Kelompok <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="ppsu_id" name="ppsu_id" required="required">
                                <option value="">-- Pilih --</option>
                            </select>
                            <?php echo form_error('ppsu_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <?php } else { ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ppsu_id">Ketua Kelompok <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="ppsu_id" name="ppsu_id" required="required">
                                <?php if($cmbPpsu!=null) { foreach($cmbPpsu as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('ppsu_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lokasi">Lokasi <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="lokasi" name="lokasi" required="required" class="form-control"></textarea>
                            <?php echo form_error('lokasi', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kondisi">Kondisi Lapangan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="kondisi" name="kondisi" required="required" class="form-control"></textarea>
                            <?php echo form_error('kondisi', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pekerjaan">Penanganan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="pekerjaan" name="pekerjaan" required="required" class="form-control"></textarea>
                            <?php echo form_error('pekerjaan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="progress">Progress Pekerjaan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="progress" name="progress" required="required" class="form-control"></textarea>
                            <?php echo form_error('progress', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="keterangan" name="keterangan" required="required" class="form-control"></textarea>
                            <?php echo form_error('keterangan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_1">Foto 0% <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="foto_1" name="foto_1" class="form-control col-md-7 col-xs-12">
                            <?php echo form_error('foto_1', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_2">Foto 50% <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="foto_2" name="foto_2" class="form-control col-md-7 col-xs-12">
                            <?php echo form_error('foto_2', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto_3">Foto 100% <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="foto_3" name="foto_3" class="form-control col-md-7 col-xs-12">
                            <?php echo form_error('foto_3', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'kegiatan'; ?>">Kembali</button>
                            <input id="btnsimpan" name="btnsimpan" value="Simpan" type="submit" class="btn btn-success" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
            
    </div>
</div>

<script>
    function getKetua(val)
    {
        if(val!='')
        {
            $.ajax({
                type:"POST",
                url:"<?php echo base_url().'kegiatan/getKetua'; ?>",
                data:{ kelurahan_id : val },
                dataType: 'json',
                success: function (data) {

                    $('#ppsu_id option[value!=""]').remove();
                    $('#ppsu_id').select2("val", null);

                    if(data.value!=null)
                    {
                        for(i=0;i<data.value.length;i++)
                        {
                            $('#ppsu_id').append('<option value="' + data.value[i] + '">' + data.text[i] + '</option>');
                        }
                    }

                }
            });
        }
        else
        {
            $('#ppsu_id option[value!=""]').remove();
        }
    }
</script>