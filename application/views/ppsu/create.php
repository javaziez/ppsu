<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Input Data PPSU</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'ppsu'; ?>">List</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'ppsu/create'; ?>" enctype="multipart/form-data">

                    <?php if($this->session->userdata('level_id') == 1) { ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required">
                                    <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                        <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                    <?php } } ?>
                                </select>
                                <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" id="kelurahan_view" name="kelurahan_view" readonly="" value="<?php echo $this->session->userdata('nama_kel'); ?>">
                                <input type="hidden" class="form-control" id="kelurahan_id" name="kelurahan_id" value="<?php echo $this->session->userdata('kelurahan_id'); ?>">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nama" name="nama" required="required" class="form-control">
                            <?php echo form_error('nama', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="alamat" name="alamat" required="required" class="form-control"></textarea>
                            <?php echo form_error('alamat', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telepon">Telepon <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="no_telepon" name="no_telepon" class="form-control">
                            <?php echo form_error('no_telepon', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ketua">Ketua <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="radio">
                                <label>
                                    <input value="1" id="ketua1" name="ketua" type="radio"> Ya
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input checked="" value="0" id="ketua2" name="ketua" type="radio"> Tidak
                                </label>
                            </div>
                            <?php echo form_error('ketua', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="foto">Foto <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="foto" name="foto" class="form-control col-md-7 col-xs-12">
                            <?php echo form_error('foto', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'ppsu'; ?>">Kembali</button>
                            <input id="btnsimpan" name="btnsimpan" value="Simpan" type="submit" class="btn btn-success" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
            
    </div>
</div>