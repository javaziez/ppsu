<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')!=1)
            redirect('login');
        
        $this->load->model('m_user');
    }
    
    public function index()
    {
        $rec = $this->m_user->listUser();
        
        $data = array(
                    'view'          => 'user/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'User',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->input->post('username')!=null)
        {
            $input = array(
                'kelurahan_id'  => $this->input->post('kelurahan_id'),
                'username'      => $this->input->post('username'),
                'password'      => md5($this->input->post('password')),
                'level_id'      => $this->input->post('level_id'),
                'aktif'         => true,
            );
            $id = $this->m_user->insertUser($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data User berhasil disimpan');

            redirect(base_url().'user/view/'.$id);
        }
        
        $data = array(
                    'view'          => 'user/create',
                    'judul'         => 'User',
                    'js'            => array('validation'=>'validation','select2'=>'select2'),
                    'css'           => array('select2'=>'select2'),
                    'cmbKelurahan'  => $this->m_user->cmbKelurahan(),
                    'cmbLevel'      => $this->m_user->cmbLevel(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function view($id)
    {
        $rec = $this->m_user->viewUser($id);
        
        $data = array(
                    'view'          => 'user/view',
                    'judul'         => 'User',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $rec = $this->m_user->viewUser($id);
        
        if($this->input->post('username')!=null)
        {
            if(trim($this->input->post('password'))!='')
            {
                $input = array(
                    'kelurahan_id'  => $this->input->post('kelurahan_id'),
                    'username'      => $this->input->post('username'),
                    'password'      => md5($this->input->post('password')),
                    'level_id'      => $this->input->post('level_id'),
                    'aktif'         => $this->input->post('aktif'),
                );
            }
            else
            {
                $input = array(
                    'kelurahan_id'  => $this->input->post('kelurahan_id'),
                    'username'      => $this->input->post('username'),
                    'level_id'      => $this->input->post('level_id'),
                    'aktif'         => $this->input->post('aktif'),
                );
            }
            $this->m_user->updateUser($input, $rec->id);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data User berhasil diubah');

            redirect(base_url().'user/view/'.$rec->id);
        }
        
        $data = array(
                    'view'          => 'user/edit',
                    'judul'         => 'User',
                    'js'            => array('validation'=>'validation','select2'=>'select2'),
                    'css'           => array('select2'=>'select2'),
                    'rec'           => $rec,
                    'cmbKelurahan'  => $this->m_user->cmbKelurahan(),
                    'cmbLevel'      => $this->m_user->cmbLevel(),
        );
        
        $this->load->view('template', $data);
    }
}