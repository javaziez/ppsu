<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>List Data User</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick=location.href="<?php echo base_url().'user/create'; ?>">Tambah</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <table id="example" class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th>No.</th>
                            <th>Kelurahan</th>
                            <th>Username</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th class=" no-link last" width="18%"><span class="nobr">Action</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($rec!=null) {  ?>
                        
                            <?php $i = 1; ?>
                        
                            <?php foreach($rec as $r) {  ?>
                        
                                <tr class="<?php echo ($i%2==0) ? 'even' : 'odd'; ?> pointer">
                                    <td class="a-center "><?php echo $i; ?></td>
                                    <td class=" "><?php echo $r->nama_kel; ?></td>
                                    <td class=" "><?php echo $r->username; ?></td>
                                    <td class=" "><?php echo $r->nama_level; ?></td>
                                    <td class=" "><?php echo ($r->aktif==true) ? 'Aktif' : 'Non-Aktif'; ?></td>
                                    <td class="a-center last">
                                        <a class="btn btn-small btn-info" href="<?php echo base_url().'user/view/'.$r->id; ?>">Detail</a> 
                                        <a class="btn btn-small btn-warning" href="<?php echo base_url().'user/edit/'.$r->id; ?>">Ubah</a>
                                    </td>
                                </tr>
                                
                                <?php $i++; ?>
                            
                            <?php } ?>
                                
                        <?php } ?>
                    </tbody>
                </table>

            </div>
        </div>
            
    </div>
</div>