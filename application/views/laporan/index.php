<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Laporan Kegiatan</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'laporan'; ?>">List</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <?php if($this->session->userdata('status')!=null) { ?>
                    <div class="alert alert-<?php echo $this->session->userdata('status'); ?> alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->userdata('pesan'); ?></strong>
                    </div>
                <?php 
                        $this->session->set_userdata('status', null);
                    }
                ?>
                
                <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'laporan/proses'; ?>" target="_blank">

                    <?php if($this->session->userdata('level_id') == 1) { ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" id="kelurahan_id" name="kelurahan_id" required="required">
                                    <?php if($cmbKelurahan!=null) { foreach($cmbKelurahan as $r) { ?>
                                        <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                    <?php } } ?>
                                </select>
                                <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="item form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" id="kelurahan_view" name="kelurahan_view" readonly="" value="<?php echo $this->session->userdata('nama_kel'); ?>">
                                <input type="hidden" class="form-control" id="kelurahan_id" name="kelurahan_id" value="<?php echo $this->session->userdata('kelurahan_id'); ?>">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_id">Kode <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="kode_id" name="kode_id" required="required">
                                <?php if($cmbKode!=null) { foreach($cmbKode as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                            </select>
                            <?php echo form_error('kode_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_laporan">Jenis Laporan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="radio">
                                <label>
                                    <input checked="" value="harian" id="jenis_laporan1" name="jenis_laporan" type="radio"> Harian
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input value="bulanan" id="jenis_laporan2" name="jenis_laporan" type="radio"> Bulanan
                                </label>
                            </div>
                            <?php echo form_error('jenis_laporan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal">Tanggal <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" class="form-control tanggal has-feedback-left" id="tanggal" name="tanggal" placeholder="Tanggal" aria-describedby="inputSuccess2Status" required="required">
                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cetak">Jenis Cetak <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="radio">
                                <label>
                                    <input checked="" value="pdf" id="cetak1" name="cetak" type="radio"> PDF
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input value="excel" id="cetak2" name="cetak" type="radio"> Excel
                                </label>
                            </div>
                            <?php echo form_error('cetak', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick=location.href="<?php echo base_url().'laporan'; ?>">Kembali</button>
                            <input id="btntampil" name="btntampil" value="Tampilkan" type="submit" class="btn btn-success" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
            
    </div>
</div>