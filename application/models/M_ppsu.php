<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_ppsu extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }

    function listPpsu()
    {
        $this->db->select('p.*, k.nama as nama_kel');
        $this->db->from('ppsu as p');
        $this->db->join('kelurahan as k', 'p.kelurahan_id=k.id', 'left');
        if($this->session->userdata('level_id')!=1)
            $this->db->where('kelurahan_id', $this->session->userdata('kelurahan_id'));
        $this->db->order_by('p.nama');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKelurahan()
    {
        $this->db->select("id, nama");
        $this->db->from('kelurahan');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertPpsu($rec)
    {
        $this->db->insert('ppsu', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewPpsu($id)
    {
        $this->db->select('p.*, kel.nama as nama_kel');
        $this->db->from('ppsu as p');
        $this->db->join('kelurahan as kel', 'p.kelurahan_id=kel.id', 'left');
        $this->db->where('p.id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updatePpsu($rec, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('ppsu', $rec);
    }
}