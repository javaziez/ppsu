<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null)
            redirect('login');
        
        $this->load->model('m_kegiatan');
    }
    
    public function index()
    {
        $rec = $this->m_kegiatan->listKegiatan();
        
        $data = array(
                    'view'          => 'kegiatan/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Kegiatan',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->input->post('kode_id')!=null)
        {
            // insert file
            $config['upload_path']          = './files/';
            $config['allowed_types']        = 'jpeg|jpg|png|pdf';
            $this->load->library('upload', $config);
            
            if($this->upload->do_upload('foto_1'))
                $foto_1 = $this->upload->data('file_name');
            else
                $foto_1 = null;
            
            if($this->upload->do_upload('foto_2'))
                $foto_2 = $this->upload->data('file_name');
            else
                $foto_2 = null;
            
            if($this->upload->do_upload('foto_3'))
                $foto_3 = $this->upload->data('file_name');
            else
                $foto_3 = null;

            $tanggal = $this->input->post('tanggal');
            $hari = date('l', strtotime($tanggal));
            $tanggal_cetak = date('d F Y', strtotime($tanggal));
            
            $input = array(
                'kelurahan_id'      => ($this->session->userdata('level_id')==1) ? $this->input->post('kelurahan_id') : $this->session->userdata('kelurahan_id'),
                'kode_id'           => $this->input->post('kode_id'),
                'sumber_id'         => $this->input->post('sumber_id'),
                'ppsu_id'           => $this->input->post('ppsu_id'),
                'tanggal'           => date("Y-m-d H:i:s", strtotime($this->input->post('tanggal'))),
                'tanggal_cetak'     => $tanggal_cetak,
                'hari'              => $hari,
                'lokasi'            => $this->input->post('lokasi'),
                'kondisi'           => $this->input->post('kondisi'),
                'pekerjaan'         => $this->input->post('pekerjaan'),
                'progress'          => $this->input->post('progress'),
                'keterangan'        => $this->input->post('keterangan'),
                'foto_1'            => $foto_1,
                'foto_2'            => $foto_2,
                'foto_3'            => $foto_3,
            );
            $id = $this->m_kegiatan->insertKegiatan($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data Kegiatan berhasil disimpan');

            redirect(base_url().'kegiatan/view/'.$id);
        }
        
        $data = array(
                    'view'          => 'kegiatan/create',
                    'judul'         => 'Kegiatan',
                    'js'            => array('validation'=>'validation','datepicker'=>'datepicker','select2'=>'select2'),
                    'css'           => array('select2'=>'select2'),
                    'cmbKelurahan'  => $this->m_kegiatan->cmbKelurahan(),
                    'cmbPpsu'       => $this->m_kegiatan->cmbPpsu(),
                    'cmbKode'       => $this->m_kegiatan->cmbKode(),
                    'cmbSumber'     => $this->m_kegiatan->cmbSumber(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function getKetua()
    {
        $kelurahan_id = $this->input->post('kelurahan_id');
        
        $rec = $this->m_kegiatan->cmbKetua($kelurahan_id);
        
        $data = array();
        if($rec!=null)
        {
            $i=0;
            foreach($rec as $r)
            {
                $data['id'][$i] = $r->id;
                $data['nama'][$i] = $r->nama;
                
                $i++;
            }
            
            echo json_encode(array(
                    'value'=>$data['id'],
                    'text'=>$data['nama'],
            ));
        }
        else
        {
            echo json_encode(array(
                    'value'=>null,
                    'text'=>null,
            ));
        }
    }
    
    public function view($id)
    {
        $rec = $this->m_kegiatan->viewKegiatan($id);
        
        $data = array(
                    'view'          => 'kegiatan/view',
                    'judul'         => 'Kegiatan',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $rec = $this->m_kegiatan->viewKegiatan($id);
        
        if($this->session->userdata('level_id')<=2 && ($this->session->userdata('level_id')==1 || $this->session->userdata('kelurahan_id')==$rec->kelurahan_id))
        {
            if($this->input->post('kode_id')!=null)
            {
                // insert file
                $config['upload_path']          = './files/';
                $config['allowed_types']        = 'jpeg|jpg|png|pdf';
                $this->load->library('upload', $config);

                if($_FILES['foto_1']['name'])
                {
                    if($this->upload->do_upload('foto_1'))
                    {
                        if(file_exists('files/'.$rec->foto_1))
                            unlink('files/'.$rec->foto_1);

                        $foto_1 = $this->upload->data('file_name');
                    }
                    else
                        $foto_1 = null;
                }
                else
                    $foto_1 = $rec->foto_1;

                if($_FILES['foto_2']['name'])
                {
                    if($this->upload->do_upload('foto_2'))
                    {
                        if(file_exists('files/'.$rec->foto_2))
                            unlink('files/'.$rec->foto_2);

                        $foto_2 = $this->upload->data('file_name');
                    }
                    else
                        $foto_2 = null;
                }
                else
                    $foto_2 = $rec->foto_2;

                if($_FILES['foto_3']['name'])
                {
                    if($this->upload->do_upload('foto_3'))
                    {
                        if(file_exists('files/'.$rec->foto_3))
                            unlink('files/'.$rec->foto_3);

                        $foto_3 = $this->upload->data('file_name');
                    }
                    else
                        $foto_3 = null;
                }
                else
                    $foto_3 = $rec->foto_3;

                $tanggal = $this->input->post('tanggal');
                $hari = date('l', strtotime($tanggal));
                $tanggal_cetak = date('d F Y', strtotime($tanggal));

                $input = array(
                    'kelurahan_id'      => ($this->session->userdata('level_id')==1) ? $this->input->post('kelurahan_id') : $this->session->userdata('kelurahan_id'),
                    'kode_id'           => $this->input->post('kode_id'),
                    'sumber_id'         => $this->input->post('sumber_id'),
                    'ppsu_id'           => $this->input->post('ppsu_id'),
                    'tanggal'           => date("Y-m-d H:i:s", strtotime($this->input->post('tanggal'))),
                    'tanggal_cetak'     => $tanggal_cetak,
                    'hari'              => $hari,
                    'lokasi'            => $this->input->post('lokasi'),
                    'kondisi'           => $this->input->post('kondisi'),
                    'pekerjaan'         => $this->input->post('pekerjaan'),
                    'progress'          => $this->input->post('progress'),
                    'keterangan'        => $this->input->post('keterangan'),
                    'foto_1'            => $foto_1,
                    'foto_2'            => $foto_2,
                    'foto_3'            => $foto_3,
                );
                $this->m_kegiatan->updateKegiatan($input, $rec->id);

                $this->session->set_userdata('status', 'info');
                $this->session->set_userdata('pesan', 'Data Kegiatan berhasil diubah');

                redirect(base_url().'kegiatan/view/'.$rec->id);
            }

            $data = array(
                        'view'          => 'kegiatan/edit',
                        'judul'         => 'Kegiatan',
                        'js'            => array('validation'=>'validation','datepicker'=>'datepicker','select2'=>'select2'),
                        'css'           => array('select2'=>'select2'),
                        'rec'           => $rec,
                        'cmbKelurahan'  => $this->m_kegiatan->cmbKelurahan(),
                        'cmbPpsu'       => $this->m_kegiatan->cmbKetua($rec->kelurahan_id),
                        'cmbKode'       => $this->m_kegiatan->cmbKode(),
                        'cmbSumber'     => $this->m_kegiatan->cmbSumber(),
            );

            $this->load->view('template', $data);
        }
        else
        {
            redirect(base_url().'kegiatan');
        }
    }
}