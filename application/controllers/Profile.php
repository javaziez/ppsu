<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null)
            redirect('login');
        
        $this->load->model('m_user');
    }
    
    public function index()
    {
        $rec = $this->m_user->viewUser($this->session->userdata('id'));
        
        $data = array(
                    'view'          => 'profile/index',
                    'judul'         => 'Profile',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit()
    {
        $rec = $this->m_user->viewUser($this->session->userdata('id'));
        
        if($this->input->post('username')!=null)
        {
            if(trim($this->input->post('password'))!='' && $this->input->post('password')==$this->input->post('password2'))
            {
                $input = array(
                    'password'      => md5($this->input->post('password')),
                );
            
                $this->m_user->updateUser($input, $this->session->userdata('id'));

                $this->session->set_userdata('status', 'info');
                $this->session->set_userdata('pesan', 'Password berhasil diubah');
                
                redirect(base_url().'profile');
            }
            else
            {
                $this->session->set_userdata('status', 'warning');
                $this->session->set_userdata('pesan', 'Terdapat Kesalahan Input');

                redirect(base_url().'profile/edit');
            }
        }
        
        $data = array(
                    'view'          => 'profile/edit',
                    'judul'         => 'Profile',
                    'js'            => array('validation'=>'validation','select2'=>'select2'),
                    'css'           => array('select2'=>'select2'),
                    'rec'           => $rec,
                    'cmbKelurahan'  => $this->m_user->cmbKelurahan(),
                    'cmbLevel'      => $this->m_user->cmbLevel(),
        );
        
        $this->load->view('template', $data);
    }
}