<div class="page-title">
    <div class="title_left">
        <h3><?php echo $judul; ?></h3>
    </div>
    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      
        <div class="x_panel">
            <div class="x_title">
                <h2>Form Tambah Data Penilaian</h2>
                <div class="pull-right">
                    <button type="button" class="btn btn-default" onclick="history.go(-1);">Kembali</button>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                <form class="form-horizontal form-label-left" novalidate method="post" action="<?php echo base_url().'penilaian/create/'.$url; ?>">

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kelurahan_id">Kelurahan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="kelurahan" name="kelurahan" value="<?php echo $nama_kel; ?>" readonly="" class="form-control" />
                            <?php echo form_error('kelurahan_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bulan">Bulan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="bulan" name="bulan" value="<?php echo $bulan; ?>" readonly="" class="form-control" />
                            <?php echo form_error('bulan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ppsu_id">PPSU <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control" id="ppsu_id" name="ppsu_id" required="required">
                                <option value="">-- Pilih PPSU --</option>
                                <?php if($cmbPpsu!=null) { foreach($cmbPpsu as $r) { ?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->nama; ?></option>
                                <?php } } ?>
                                <?php echo form_error('ppsu_id', '<span class="help-block" style="color: red">', '</span>'); ?>
                            </select>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alpha">Alpha <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="alpha" name="alpha" class="form-control" value="<?php echo set_value('alpha'); ?>" required="">
                            <?php echo form_error('alpha', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sakit">Sakit <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="sakit" name="sakit" class="form-control" value="<?php echo set_value('sakit'); ?>" required="">
                            <?php echo form_error('sakit', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="izin">Izin <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="izin" name="izin" class="form-control" value="<?php echo set_value('izin'); ?>" required="">
                            <?php echo form_error('izin', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nilai_1">Disiplin Kehadiran <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="nilai_1" name="nilai_1" class="form-control" value="<?php echo set_value('nilai_1'); ?>" required="">
                            <?php echo form_error('nilai_1', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nilai_2">Tanggung Jawab Penyelesaian Pekerjaan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="nilai_2" name="nilai_2" class="form-control" value="<?php echo set_value('nilai_2'); ?>" required="">
                            <?php echo form_error('nilai_2', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nilai_3">Kepatuhan terhadap Kewajiban dan Larangan <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="number" id="nilai_3" name="nilai_3" class="form-control" value="<?php echo set_value('nilai_3'); ?>" required="">
                            <?php echo form_error('nilai_3', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea id="keterangan" name="keterangan" class="form-control"><?php echo set_value('keterangan'); ?></textarea>
                            <?php echo form_error('keterangan', '<span class="help-block" style="color: red">', '</span>'); ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button" class="btn btn-default" onclick="history.go(-1);">Kembali</button>
                            <input id="btnsimpan" name="btnsimpan" value="Simpan" type="submit" class="btn btn-success" />
                        </div>
                    </div>
                </form>

            </div>
        </div>
            
    </div>
</div>