<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    
class m_user extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->database('default');
    }
    
    function cekLogin($username, $password)
    {
        $this->db->select('u.*, l.nama as nama_level, k.nama as nama_kel');
        $this->db->from('user as u');
        $this->db->join('level as l', 'u.level_id=l.id', 'left');
        $this->db->join('kelurahan as k', 'u.kelurahan_id=k.id', 'left');
        $this->db->where('username', $username);
        $this->db->where('password', md5($password));
        $this->db->where('aktif', 1);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function listUser()
    {
        $this->db->select('u.*, l.nama as nama_level, k.nama as nama_kel');
        $this->db->from('user as u');
        $this->db->join('level as l', 'u.level_id=l.id', 'left');
        $this->db->join('kelurahan as k', 'u.kelurahan_id=k.id', 'left');
        $this->db->where('level_id!=4');
        $this->db->order_by('u.id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbKelurahan()
    {
        $this->db->select('*');
        $this->db->from('kelurahan');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function cmbLevel()
    {
        $this->db->select('*');
        $this->db->from('level');
        $this->db->order_by('id');
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->result();
        else
            return null;
    }
    
    function insertUser($rec)
    {
        $this->db->insert('user', $rec);
        
        return $this->db->insert_id();
    }
    
    function viewUser($id)
    {
        $this->db->select('u.*, l.nama as nama_level, k.nama as nama_kel');
        $this->db->from('user as u');
        $this->db->join('level as l', 'u.level_id=l.id', 'left');
        $this->db->join('kelurahan as k', 'u.kelurahan_id=k.id', 'left');
        $this->db->where('u.id', $id);
        $rec = $this->db->get();
        
        if ($rec->num_rows() > 0)
            return $rec->row();
        else
            return null;
    }
    
    function updateUser($r, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $r);
    }
    
    function resetPassword($r, $nip)
    {
        $this->db->where('nip', $nip);
        $this->db->update('user', $r);
    }
    
    function ubahProfile($r, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $r);
    }
    
    function loginDate($r)
    {
        $this->db->where('id', $this->session->userdata('id'));
        $this->db->update('admin', $r);
    }
    
    function insertLog($r)
    {
        $this->db->insert('lastlogin', $r);
    }
}