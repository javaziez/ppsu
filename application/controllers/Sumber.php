<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sumber extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        if($this->session->userdata('id')==null || $this->session->userdata('level_id')!=1)
            redirect('login');
        
        $this->load->model('m_sumber');
    }
    
    public function index()
    {
        $rec = $this->m_sumber->listSumber();
        
        $data = array(
                    'view'          => 'sumber/index',
                    'js'            => array('datatables'=>'datatables'),
                    'css'           => array('datatables'=>'datatables'),
                    'judul'         => 'Sumber',
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function create()
    {
        if($this->input->post('nama')!=null)
        {
            $input = array(
                'nama'          => $this->input->post('nama'),
            );
            $id = $this->m_sumber->insertSumber($input);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data Sumber berhasil disimpan');

            redirect(base_url().'sumber/view/'.$id);
        }
        
        $data = array(
                    'view'          => 'sumber/create',
                    'judul'         => 'Sumber',
                    'js'            => array('validation'=>'validation'),
                    'css'           => array(),
        );
        
        $this->load->view('template', $data);
    }
    
    public function view($id)
    {
        $rec = $this->m_sumber->viewSumber($id);
        
        $data = array(
                    'view'          => 'sumber/view',
                    'judul'         => 'Sumber',
                    'js'            => array(),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
    
    public function edit($id)
    {
        $rec = $this->m_sumber->viewSumber($id);
        
        if($this->input->post('nama')!=null)
        {
            $input = array(
                'nama'          => $this->input->post('nama'),
            );
            $this->m_sumber->updateSumber($input, $rec->id);
            
            $this->session->set_userdata('status', 'info');
            $this->session->set_userdata('pesan', 'Data Sumber berhasil diubah');

            redirect(base_url().'sumber/view/'.$rec->id);
        }
        
        $data = array(
                    'view'          => 'sumber/edit',
                    'judul'         => 'Sumber',
                    'js'            => array('validation'=>'validation'),
                    'css'           => array(),
                    'rec'           => $rec,
        );
        
        $this->load->view('template', $data);
    }
}